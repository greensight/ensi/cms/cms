<?php

namespace App\Providers;

use App\Domain\Contents\Models\Banner;
use App\Domain\Contents\Observers\BannerObserver;
use App\Domain\Nameplates\Models\Nameplate;
use App\Domain\Nameplates\Models\NameplateProduct;
use App\Domain\Nameplates\Observers\NameplateAfterCommitObserver;
use App\Domain\Nameplates\Observers\NameplateObserver;
use App\Domain\Nameplates\Observers\NameplateProductAfterCommitObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [];

    public function boot(): void
    {
        parent::boot();

        Banner::observe(BannerObserver::class);
        Nameplate::observe(NameplateObserver::class);
        Nameplate::observe(NameplateAfterCommitObserver::class);
        NameplateProduct::observe(NameplateProductAfterCommitObserver::class);
    }

    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
