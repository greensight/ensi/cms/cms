<?php

namespace App\Domain\Contents\Models;

use App\Domain\Contents\Models\Tests\Factories\BannerTypeFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\BannerTypeEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name Type name
 * @property BannerTypeEnum $code Type code
 * @property bool $active Activity type
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class BannerType extends Model
{
    protected $table = 'banner_types';

    protected $casts = [
        'code' => BannerTypeEnum::class,
        'active' => 'bool',
    ];

    public static function factory(): BannerTypeFactory
    {
        return BannerTypeFactory::new();
    }
}
