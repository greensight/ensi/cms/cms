<?php

namespace App\Domain\Contents\Models\Tests\Factories;

use App\Domain\Contents\Models\BannerButton;
use App\Http\ApiV1\OpenApiGenerated\Enums\BannerButtonLocationEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BannerButtonTypeEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class BannerButtonFactory extends BaseModelFactory
{
    protected $model = BannerButton::class;

    public function definition(): array
    {
        return [
            'url' => $this->faker->url,
            'text' => $this->faker->text,
            'location' => $this->faker->randomEnum(BannerButtonLocationEnum::cases()),
            'type' => $this->faker->randomEnum(BannerButtonTypeEnum::cases()),
        ];
    }
}
