<?php

namespace App\Domain\Contents\Models\Tests\Factories;

use App\Domain\Contents\Models\Banner;
use App\Domain\Contents\Models\BannerButton;
use App\Domain\Contents\Models\BannerType;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Illuminate\Support\Str;

class BannerFactory extends BaseModelFactory
{
    protected $model = Banner::class;

    public function definition(): array
    {
        $name = $this->faker->words(3, true);

        return [
            'name' => $name,
            'code' => Str::slug($name),
            'is_active' => $this->faker->boolean,
            'desktop_image' => $this->faker->optional()->url,
            'mobile_image' => $this->faker->optional()->url,
            'url' => $this->faker->optional()->url,
            'sort' => $this->faker->numberBetween(),
            'type_id' => BannerType::factory(),
            'button_id' => BannerButton::factory(),
        ];
    }

    public function active(): static
    {
        return $this->state([
            'is_active' => true,
        ]);
    }

    public function inactive(): static
    {
        return $this->state([
            'is_active' => false,
        ]);
    }
}
