<?php

namespace App\Domain\Contents\Models\Tests\Factories;

use App\Domain\Nameplates\Models\Nameplate;
use App\Domain\Nameplates\Models\NameplateProduct;
use Ensi\LaravelTestFactories\BaseModelFactory;

class NameplateProductFactory extends BaseModelFactory
{
    protected $model = NameplateProduct::class;

    public function definition()
    {
        return [
            'nameplate_id' => Nameplate::factory(),
            'product_id' => $this->faker->modelId(),
        ];
    }
}
