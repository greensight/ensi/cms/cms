<?php

namespace App\Domain\Contents\Models\Tests\Factories;

use App\Domain\Contents\Models\BannerType;
use App\Http\ApiV1\OpenApiGenerated\Enums\BannerTypeEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class BannerTypeFactory extends BaseModelFactory
{
    protected $model = BannerType::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->words(3, true),
            'code' => $this->faker->randomEnum(BannerTypeEnum::cases()),
            'active' => $this->faker->boolean,
        ];
    }

    public function active(): static
    {
        return $this->state([
            'active' => true,
        ]);
    }

    public function inactive(): static
    {
        return $this->state([
            'active' => false,
        ]);
    }
}
