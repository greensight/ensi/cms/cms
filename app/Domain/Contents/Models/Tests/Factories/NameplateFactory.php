<?php

namespace App\Domain\Contents\Models\Tests\Factories;

use App\Domain\Nameplates\Models\Nameplate;
use Ensi\LaravelTestFactories\BaseModelFactory;

class NameplateFactory extends BaseModelFactory
{
    protected $model = Nameplate::class;

    public function definition()
    {
        return [
            'name' => $this->faker->text(50),
            'code' => $this->faker->unique()->text(50),

            'background_color' => $this->faker->hexColor(),
            'text_color' => $this->faker->hexColor(),

            'is_active' => $this->faker->boolean(),
        ];
    }
}
