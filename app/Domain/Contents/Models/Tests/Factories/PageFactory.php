<?php

namespace App\Domain\Contents\Models\Tests\Factories;

use App\Domain\Contents\Models\Page;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Illuminate\Support\Carbon;

class PageFactory extends BaseModelFactory
{
    protected $model = Page::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->words(3, true),
            'slug' => $this->faker->slug(),
            'content' => $this->faker->randomHtml(),
            'is_active' => $this->faker->boolean(),
            'active_from' => Carbon::yesterday(),
            'active_to' => Carbon::tomorrow(),
        ];
    }

    public function active(): static
    {
        return $this->state([
            'is_active' => true,
            'active_from' => Carbon::yesterday(),
            'active_to' => Carbon::tomorrow(),
        ]);
    }

    public function inactive(): static
    {
        return $this->state([
            'is_active' => true,
            'active_from' => Carbon::tomorrow(),
            'active_to' => Carbon::tomorrow(),
        ]);
    }
}
