<?php

namespace App\Domain\Contents\Models;

use App\Domain\Contents\Models\Tests\Factories\BannerButtonFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\BannerButtonLocationEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BannerButtonTypeEnum;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id - id кнопки
 * @property string $url - ссылка
 * @property string $text - текст кнопке
 * @property BannerButtonLocationEnum $location - размещение кнопки на баннере
 * @property BannerButtonTypeEnum $type - тип кнопки
 */
class BannerButton extends Model
{
    protected $table = 'banner_buttons';

    protected $fillable = [
        'url',
        'text',
        'location',
        'type',
    ];

    protected $casts = [
        'location' => BannerButtonLocationEnum::class,
        'type' => BannerButtonTypeEnum::class,
    ];

    public static function factory(): BannerButtonFactory
    {
        return BannerButtonFactory::new();
    }
}
