<?php

namespace App\Domain\Contents\Models;

use App\Domain\Contents\Models\Tests\Factories\PageFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;

/**
 * @property int $id - id страницы
 * @property string $name - имя
 * @property string $slug - человекопонятный идентификатор для url
 * @property string $content - html блок-контент
 *
 * @property bool $is_active - активность страницы
 * @property CarbonInterface $active_from - дата начала публикации страницы
 * @property CarbonInterface $active_to - дата окончания публикации страницы
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at

 */
class Page extends Model
{
    protected $table = 'pages';

    protected $fillable = [
        'name',
        'slug',
        'content',
        'is_active',
        'active_from',
        'active_to',
    ];

    protected $casts = [
        'is_active' => 'bool',
        'active_from' => 'datetime',
        'active_to' => 'datetime',
    ];

    public function scopeIsVisible(Builder $query, bool $value): Builder
    {
        $now = Date::now();

        if ($value) {
            return $query->where('is_active', true)
                ->where(function (Builder $query) use ($now) {
                    return $query->where('active_from', '<=', $now)
                        ->orWhereNull('active_from');
                })
                ->where(function (Builder $query) use ($now) {
                    return $query->where('active_to', '>=', $now)
                        ->orWhereNull('active_to');
                });
        }

        return $query->where('is_active', false)
            ->orWhere('active_from', '>', $now)
            ->orWhere('active_to', '<', $now);
    }

    public static function factory(): PageFactory
    {
        return PageFactory::new();
    }
}
