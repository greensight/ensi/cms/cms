<?php

namespace App\Domain\Contents\Models;

use App\Domain\Contents\Models\Tests\Factories\BannerFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id - id баннера
 * @property int|null $type_id - id типа баннера
 * @property int|null $button_id - id кнопки банера
 *
 * @property string $name - имя
 * @property string $code - код
 * @property bool $is_active - активность баннера
 *
 * @property string $desktop_image - изображение баннера
 * @property string $mobile_image - изображение для адаптива

 * @property string|null $url - ссылка на веб страницу для перехода по баннеру
 * @property int $sort - сортировка (Определяет порядок вывода баннера)
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read BannerType|null $type
 * @property-read BannerButton|null $button
 */
class Banner extends Model
{
    public const DEFAULT_SORT = 500;

    protected $table = 'banners';

    protected $fillable = [
        'name',
        'is_active',
        'url',
        'sort',
        'type_id',
    ];

    protected $casts = [
        'is_active' => 'bool',
        'sort' => 'int',
    ];

    protected $attributes = [
        'sort' => self::DEFAULT_SORT,
    ];

    public function type(): BelongsTo
    {
        return $this->belongsTo(BannerType::class, 'type_id', 'id');
    }

    public function button(): BelongsTo
    {
        return $this->belongsTo(BannerButton::class, 'button_id', 'id');
    }

    public static function factory(): BannerFactory
    {
        return BannerFactory::new();
    }
}
