<?php

namespace App\Domain\Contents\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Kalnoy\Nestedset\Collection as NestedCollection;

/**
 * @property int $id - id меню
 * @property string $name - имя меню
 * @property string $code - код меню
 *
 * @property-read NestedCollection|MenuItem[] $items - элементы меню
 */
class Menu extends Model
{
    protected $table = 'menus';

    public const HEADER_MAIN_MENU_CODE = 'header_main';
    public const HEADER_HELP_MENU_CODE = 'header_help';
    public const FOOTER_MAIN_MENU_CODE = 'footer_main';

    public function items(): HasMany
    {
        return $this->hasMany(MenuItem::class);
    }

    /**
     * Элементы меню в виде дерева
     */
    public function getItemsTree(): NestedCollection
    {
        return $this->items->toTree();
    }
}
