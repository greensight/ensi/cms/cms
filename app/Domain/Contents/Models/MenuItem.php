<?php

namespace App\Domain\Contents\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Kalnoy\Nestedset\NodeTrait;

/**
 * @property int $id
 * @property string $url
 * @property string $name
 * @property int $menu_id
 * @property int $_lft
 * @property int $_rgt
 * @property int $parent_id
 * @property bool $active
 * @property array $options
 * @property array $children
 */
class MenuItem extends Model
{
    use NodeTrait;

    protected $table = 'menu_items';

    protected $fillable = [
        'url',
        'name',
        'menu_id',
        '_lft',
        '_rgt',
        'parent_id',
        'options',
    ];

    public function menu(): HasOne
    {
        return $this->hasOne(Menu::class);
    }

    protected function getScopeAttributes(): array
    {
        return ['menu_id'];
    }
}
