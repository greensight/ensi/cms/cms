<?php

namespace App\Domain\Contents\Actions\Pages;

use App\Domain\Contents\Models\Page;

class CreatePageAction
{
    public function execute(array $fields): Page
    {
        $page = new Page();
        $page->fill($fields);
        $page->save();

        return $page;
    }
}
