<?php

namespace App\Domain\Contents\Actions\Pages;

use App\Domain\Contents\Models\Page;

class PatchPageAction
{
    public function execute(int $id, array $fields): Page
    {
        /** @var Page $page */
        $page = Page::query()->findOrFail($id);
        $page->fill($fields);
        $page->save();

        return $page;
    }
}
