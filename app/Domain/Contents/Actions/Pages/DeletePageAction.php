<?php

namespace App\Domain\Contents\Actions\Pages;

use App\Domain\Contents\Models\Page;

class DeletePageAction
{
    public function execute(int $id): void
    {
        Page::destroy($id);
    }
}
