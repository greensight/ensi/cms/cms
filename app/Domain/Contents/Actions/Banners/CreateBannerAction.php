<?php

namespace App\Domain\Contents\Actions\Banners;

use App\Domain\Contents\Models\Banner;
use Illuminate\Support\Str;

class CreateBannerAction
{
    public function __construct(protected readonly CreateBannerButtonAction $createBannerButtonAction)
    {
    }

    public function execute(array $fields, ?array $button): Banner
    {
        $bannerModel = new Banner();
        $bannerModel->fill($fields);
        $bannerModel->code = Str::slug($bannerModel->name);

        if ($button !== null) {
            $bannerButton = $this->createBannerButtonAction->execute($button);
            $bannerModel->button_id = $bannerButton->id;
        }

        $bannerModel->save();

        return $bannerModel->load(['button']);
    }
}
