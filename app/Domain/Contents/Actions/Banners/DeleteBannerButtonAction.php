<?php

namespace App\Domain\Contents\Actions\Banners;

use App\Domain\Contents\Models\BannerButton;

class DeleteBannerButtonAction
{
    public function execute(int $id): void
    {
        BannerButton::destroy($id);
    }
}
