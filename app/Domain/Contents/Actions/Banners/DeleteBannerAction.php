<?php

namespace App\Domain\Contents\Actions\Banners;

use App\Domain\Contents\Models\Banner;

class DeleteBannerAction
{
    public function execute(int $id): void
    {
        Banner::destroy($id);
    }
}
