<?php

namespace App\Domain\Contents\Actions\Banners;

use App\Domain\Contents\Models\BannerButton;

class CreateBannerButtonAction
{
    public function execute(array $fields): BannerButton
    {
        $bannerButton = new BannerButton();
        $bannerButton->fill($fields);
        $bannerButton->save();

        return $bannerButton;
    }
}
