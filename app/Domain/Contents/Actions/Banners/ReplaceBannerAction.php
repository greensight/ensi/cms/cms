<?php

namespace App\Domain\Contents\Actions\Banners;

use App\Domain\Contents\Models\Banner;
use App\Domain\Contents\Models\BannerButton;
use Illuminate\Support\Str;

class ReplaceBannerAction
{
    public function __construct(
        protected readonly CreateBannerButtonAction $createBannerButtonAction,
        protected readonly ReplaceBannerButtonAction $replaceBannerButtonAction,
    ) {
    }

    public function execute(int $id, array $fields, ?array $button): Banner
    {
        /** @var Banner $bannerModel */
        $bannerModel = Banner::query()->where('id', $id)->firstOrFail();
        $bannerModel->fill($fields);
        $bannerModel->code = Str::slug($bannerModel->name);

        $bannerButtonId = null;
        if ($button !== null) {
            $bannerButtonId = $this->upsertBannerButton($bannerModel->button_id, $button)->id;
        }
        $bannerModel->button_id = $bannerButtonId;

        $bannerModel->save();

        return $bannerModel->load(['button']);
    }

    protected function upsertBannerButton(?int $buttonId, array $button): BannerButton
    {
        return $buttonId === null ?
            $this->createBannerButtonAction->execute($button) :
            $this->replaceBannerButtonAction->execute($buttonId, $button);
    }
}
