<?php

namespace App\Domain\Contents\Actions\Banners;

use App\Domain\Contents\Models\BannerButton;

class ReplaceBannerButtonAction
{
    public function execute(int $id, array $fields): BannerButton
    {
        /** @var BannerButton $bannerButton */
        $bannerButton = BannerButton::query()->findOrFail($id);
        $bannerButton->fill($fields);
        $bannerButton->save();

        return $bannerButton;
    }
}
