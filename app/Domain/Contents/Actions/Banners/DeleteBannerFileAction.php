<?php

namespace App\Domain\Contents\Actions\Banners;

use App\Domain\Contents\Models\Banner;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Storage;

class DeleteBannerFileAction
{
    public function __construct(protected EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(int $id, string $type): void
    {
        /** @var Banner $banner */
        $banner = Banner::query()->findOrFail($id);

        $column = "{$type}_image";

        $disk = Storage::disk($this->fileManager->publicDiskName());

        if ($banner[$column]) {
            $disk->delete($banner[$column]);
        }

        $banner[$column] = null;
        $banner->save();
    }
}
