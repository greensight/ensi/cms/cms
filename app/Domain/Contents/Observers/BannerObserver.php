<?php

namespace App\Domain\Contents\Observers;

use App\Domain\Contents\Actions\Banners\DeleteBannerButtonAction;
use App\Domain\Contents\Models\Banner;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Storage;

class BannerObserver
{
    public function __construct(
        protected readonly EnsiFilesystemManager $filesystemManager,
        protected readonly DeleteBannerButtonAction $deleteBannerButtonAction,
    ) {
    }

    public function updated(Banner $banner): void
    {
        if ($banner->isDirty('button_id') && $banner->getOriginal('button_id')) {
            $this->deleteBannerButtonAction->execute($banner->getOriginal('button_id'));
        }
    }

    public function deleted(Banner $banner): void
    {
        $buttonId = $banner->getOriginal('button_id');
        if ($buttonId !== null) {
            $this->deleteBannerButtonAction->execute($buttonId);
        }

        $this->deleteFile($banner->desktop_image);
        $this->deleteFile($banner->mobile_image);
    }

    protected function deleteFile(?string $path): void
    {
        $disk = Storage::disk($this->filesystemManager->publicDiskName());
        if ($path) {
            $disk->delete($path);
        }
    }
}
