<?php

namespace App\Domain\Nameplates\Models;

use App\Domain\Contents\Models\Tests\Factories\NameplateFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id - идентификатор тега
 *
 * @property string $name - наименование тега
 * @property string $code - код тега
 *
 * @property string $background_color - цвет фона
 * @property string $text_color - цвет текста
 *
 * @property bool $is_active - активность
 *
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property-read Collection|NameplateProduct[] $products - ссылки на товары в PIM
 */
class Nameplate extends Model
{
    protected $table = 'nameplates';

    protected $fillable = [
        'name',
        'code',
        'background_color',
        'text_color',
        'is_active',
    ];

    protected $casts = [
        'is_active' => 'bool',
    ];

    public function products(): HasMany
    {
        return $this->hasMany(NameplateProduct::class);
    }

    public static function factory(): NameplateFactory
    {
        return NameplateFactory::new();
    }
}
