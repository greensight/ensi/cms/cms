<?php

namespace App\Domain\Nameplates\Models;

use App\Domain\Contents\Models\Tests\Factories\NameplateProductFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id - идентификатор связи
 * @property int $nameplate_id - идентификатор тега
 * @property int $product_id - идентификатор товара
 *
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property-read Nameplate $nameplate - модель тега
 */
class NameplateProduct extends Model
{
    protected $table = 'nameplate_products';

    public function nameplate(): BelongsTo
    {
        return $this->belongsTo(Nameplate::class);
    }

    public static function factory(): NameplateProductFactory
    {
        return NameplateProductFactory::new();
    }
}
