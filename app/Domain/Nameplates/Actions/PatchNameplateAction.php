<?php

namespace App\Domain\Nameplates\Actions;

use App\Domain\Nameplates\Models\Nameplate;

class PatchNameplateAction
{
    public function execute(int $id, array $fields): Nameplate
    {
        /** @var Nameplate $nameplate */
        $nameplate = Nameplate::query()->findOrFail($id);
        $nameplate->fill($fields);
        $nameplate->save();

        return $nameplate;
    }
}
