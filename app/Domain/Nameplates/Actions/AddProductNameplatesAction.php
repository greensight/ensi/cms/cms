<?php

namespace App\Domain\Nameplates\Actions;

use App\Domain\Nameplates\Models\NameplateProduct;
use App\Exceptions\ValidateException;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\ApiException as PimApiException;
use Illuminate\Support\Facades\DB;

class AddProductNameplatesAction
{
    public function __construct(protected ProductsApi $productsApi)
    {
    }

    public function execute(int $productId, array $nameplateIds): void
    {
        $this->validateProduct($productId);

        DB::transaction(function () use ($productId, $nameplateIds) {
            foreach ($nameplateIds as $nameplateId) {
                $nameplateProduct = new NameplateProduct();
                $nameplateProduct->product_id = $productId;
                $nameplateProduct->nameplate_id = $nameplateId;
                $nameplateProduct->save();
            }
        });
    }

    private function validateProduct(int $productId): void
    {
        try {
            $this->productsApi->getProduct($productId)->getData();
        } catch (PimApiException $e) {
            throw $e->getCode() == 404 ?
                new ValidateException('Not found', $e->getCode()) :
                new ValidateException("Error occurred while fetching product, error code {$e->getCode()}");
        }
    }
}
