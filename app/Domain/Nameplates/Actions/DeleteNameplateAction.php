<?php

namespace App\Domain\Nameplates\Actions;

use App\Domain\Nameplates\Models\Nameplate;

class DeleteNameplateAction
{
    public function execute(int $id): void
    {
        /** @var Nameplate $nameplate */
        $nameplate = Nameplate::query()->findOrFail($id);
        $nameplate->delete();
    }
}
