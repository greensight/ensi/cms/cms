<?php

namespace App\Domain\Nameplates\Actions;

use App\Domain\Nameplates\Models\Nameplate;

class CreateNameplateAction
{
    public function execute(array $fields): Nameplate
    {
        $nameplate = new Nameplate();
        $nameplate->fill($fields);
        $nameplate->save();

        return $nameplate;
    }
}
