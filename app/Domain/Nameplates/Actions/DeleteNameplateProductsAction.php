<?php

namespace App\Domain\Nameplates\Actions;

use App\Domain\Nameplates\Models\NameplateProduct;
use Illuminate\Support\Facades\DB;

class DeleteNameplateProductsAction
{
    public function execute(int $nameplateId, array $productIds): void
    {
        $nameplateProductLinks = NameplateProduct::query()
            ->where('nameplate_id', $nameplateId)
            ->whereIn('product_id', $productIds)
            ->get();

        DB::transaction(function () use ($nameplateProductLinks) {
            $nameplateProductLinks->each(fn (NameplateProduct $nameplateProductLink) => $nameplateProductLink->delete());
        });
    }
}
