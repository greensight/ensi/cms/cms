<?php

namespace App\Domain\Nameplates\Actions;

use App\Domain\Nameplates\Models\Nameplate;
use App\Domain\Nameplates\Models\NameplateProduct;
use App\Domain\Support\Actions\ValidateProductsExistAction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Webmozart\Assert\Assert;

class AddNameplateProductsAction
{
    public function __construct(protected ValidateProductsExistAction $validateProducts)
    {
    }

    public function execute(int $nameplateId, Builder $query, array $productIds): void
    {
        Assert::isAOf($query->getModel(), Nameplate::class);
        $query->findOrFail($nameplateId); // The variable itself is not needed here, but validation is required

        $this->validateProducts->execute($productIds);

        DB::transaction(function () use ($nameplateId, $productIds) {
            foreach ($productIds as $productId) {
                $nameplateProductLink = new NameplateProduct();
                $nameplateProductLink->product_id = $productId;
                $nameplateProductLink->nameplate_id = $nameplateId;
                $nameplateProductLink->save();
            }
        });
    }
}
