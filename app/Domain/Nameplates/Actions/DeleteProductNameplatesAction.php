<?php

namespace App\Domain\Nameplates\Actions;

use App\Domain\Nameplates\Models\NameplateProduct;
use Illuminate\Support\Facades\DB;

class DeleteProductNameplatesAction
{
    public function execute(int $productId, array $nameplateIds): void
    {
        $nameplateProducts = NameplateProduct::query()
            ->where('product_id', $productId)
            ->whereIn('nameplate_id', $nameplateIds)
            ->get();

        DB::transaction(function () use ($nameplateProducts) {
            $nameplateProducts->each(fn (NameplateProduct $nameplateProduct) => $nameplateProduct->delete());
        });
    }
}
