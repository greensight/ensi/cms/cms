<?php

namespace App\Domain\Nameplates\Observers;

use App\Domain\Kafka\Actions\Send\SendNameplateProductEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Nameplates\Models\NameplateProduct;

class NameplateProductAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(protected SendNameplateProductEventAction $eventAction)
    {
    }

    public function created(NameplateProduct $model): void
    {
        $this->eventAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(NameplateProduct $model): void
    {
        $this->eventAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(NameplateProduct $model): void
    {
        $this->eventAction->execute($model, ModelEventMessage::DELETE);
    }
}
