<?php

namespace App\Domain\Nameplates\Observers;

use App\Domain\Nameplates\Models\Nameplate;
use App\Domain\Nameplates\Models\NameplateProduct;

class NameplateObserver
{
    public function deleting(Nameplate $model): void
    {
        $model->loadMissing('products');
        $model->products->each(fn (NameplateProduct $link) => $link->delete());
    }
}
