<?php

namespace App\Domain\Nameplates\Observers;

use App\Domain\Kafka\Actions\Send\SendNameplateEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Nameplates\Models\Nameplate;

class NameplateAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(protected SendNameplateEventAction $eventAction)
    {
    }

    public function created(Nameplate $model): void
    {
        $this->eventAction->execute($model, ModelEventMessage::CREATE);
    }

    public function updated(Nameplate $model): void
    {
        $this->eventAction->execute($model, ModelEventMessage::UPDATE);
    }

    public function deleted(Nameplate $model): void
    {
        $this->eventAction->execute($model, ModelEventMessage::DELETE);
    }
}
