<?php

namespace App\Domain\Seo\Actions\Templates;

use App\Domain\Seo\Models\SeoTemplate;

class DeleteSeoTemplateAction
{
    public function execute(int $id): void
    {
        SeoTemplate::destroy($id);
    }
}
