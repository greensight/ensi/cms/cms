<?php

namespace App\Domain\Seo\Actions\Templates;

use App\Domain\Seo\Actions\TemplateProducts\DeactivateDefaultProductTemplateAction;
use App\Domain\Seo\Models\SeoTemplate;
use Illuminate\Support\Facades\DB;

class SaveSeoTemplateAction
{
    public function __construct(protected DeactivateDefaultProductTemplateAction $deactivateProductTemplateAction)
    {
    }

    public function execute(SeoTemplate $template): SeoTemplate
    {
        return DB::transaction(function () use ($template) {

            if ($template->isDefaultProduct() && $template->isDirty('is_active')) {
                $this->deactivateProductTemplateAction->execute();
            }

            if ($template->isDirty()) {
                $template->save();
            }

            return $template;
        });
    }
}
