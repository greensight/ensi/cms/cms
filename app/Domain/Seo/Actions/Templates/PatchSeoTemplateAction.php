<?php

namespace App\Domain\Seo\Actions\Templates;

use App\Domain\Seo\Models\SeoTemplate;

class PatchSeoTemplateAction
{
    public function __construct(protected SaveSeoTemplateAction $save)
    {
    }

    public function execute(int $id, array $fields): SeoTemplate
    {
        /** @var SeoTemplate $seoTemplate */
        $seoTemplate = SeoTemplate::query()->findOrFail($id);
        $seoTemplate->fill($fields);

        return $this->save->execute($seoTemplate);
    }
}
