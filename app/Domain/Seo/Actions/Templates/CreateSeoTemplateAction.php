<?php

namespace App\Domain\Seo\Actions\Templates;

use App\Domain\Seo\Models\SeoTemplate;

class CreateSeoTemplateAction
{
    public function __construct(protected SaveSeoTemplateAction $save)
    {
    }

    public function execute(array $fields): SeoTemplate
    {
        $seoTemplate = new SeoTemplate();
        $seoTemplate->fill($fields);

        return $this->save->execute($seoTemplate);
    }
}
