<?php

namespace App\Domain\Seo\Actions\TemplateProducts;

use App\Domain\Seo\Models\SeoTemplateProduct;
use Illuminate\Support\Facades\DB;

class DeleteProductsAction
{
    public function execute(int $templateId, array $productIds): void
    {
        $templateProducts = SeoTemplateProduct::query()
            ->where('template_id', $templateId)
            ->whereIn('product_id', $productIds)
            ->get();

        DB::transaction(function () use ($templateProducts) {
            $templateProducts->each(fn (SeoTemplateProduct $templateProduct) => $templateProduct->delete());
        });
    }
}
