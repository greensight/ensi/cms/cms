<?php

namespace App\Domain\Seo\Actions\TemplateProducts;

use App\Domain\Seo\Models\SeoTemplate;
use App\Http\ApiV1\OpenApiGenerated\Enums\SeoTemplateTypeEnum;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class DeactivateDefaultProductTemplateAction
{
    public function execute(): void
    {
        /** @var Collection<SeoTemplate> $templates */
        $templates = SeoTemplate::query()
            ->where('type', SeoTemplateTypeEnum::PRODUCT_DEFAULT)
            ->get();

        DB::transaction(function () use ($templates) {
            $templates->each(fn (SeoTemplate $template) => $template->deactivate());
        });
    }
}
