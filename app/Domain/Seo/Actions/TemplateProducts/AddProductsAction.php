<?php

namespace App\Domain\Seo\Actions\TemplateProducts;

use App\Domain\Seo\Models\SeoTemplate;
use App\Domain\Seo\Models\SeoTemplateProduct;
use App\Domain\Support\Actions\ValidateProductsExistAction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Webmozart\Assert\Assert;

class AddProductsAction
{
    public function __construct(protected ValidateProductsExistAction $validateProducts)
    {
    }

    public function execute(int $templateId, Builder $query, array $productIds): void
    {

        Assert::isAOf($query->getModel(), SeoTemplate::class);
        $query->findOrFail($templateId); // The variable itself is not needed here, but validation is required

        $this->validateProducts->execute($productIds);

        DB::transaction(function () use ($templateId, $productIds) {
            foreach ($productIds as $productId) {
                $templateProduct = new SeoTemplateProduct();
                $templateProduct->product_id = $productId;
                $templateProduct->template_id = $templateId;
                $templateProduct->save();
            }
        });
    }
}
