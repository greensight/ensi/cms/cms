<?php

namespace App\Domain\Seo\Actions\SeoVariables;

use App\Domain\Seo\Contracts\LoadLazyVariableAction;
use App\Domain\Seo\Contracts\VariableValueData;
use App\Domain\Seo\Models\SeoTemplate;
use App\Http\ApiV1\OpenApiGenerated\Enums\SeoVariableEnum;

class SetSeoVariablesAction
{
    protected VariableValueData $variableValue;

    public function execute(SeoTemplate $template, LoadLazyVariableAction $loadVariableAction): SeoTemplate
    {
        if (!$this->isExistVariables($template)) {
            return $template;
        }

        $this->variableValue = $loadVariableAction->load();

        return $this->replaceVariables($template);
    }

    protected function isExistVariables(SeoTemplate $template): bool
    {
        foreach (SeoTemplate::supportSeoVariable() as $field) {
            $filedModel = $template->getAttribute($field);

            foreach (SeoVariableEnum::cases() as $variable) {
                if (str_contains($filedModel, $variable->value)) {
                    return true;
                }
            }
        }

        return false;
    }

    protected function replaceVariables(SeoTemplate $template): SeoTemplate
    {
        foreach (SeoTemplate::supportSeoVariable() as $field) {
            $fieldValue = $template->getAttribute($field);
            if (is_string($fieldValue)) {
                $template->setAttribute($field, $this->replaceVariablesForField($fieldValue));
            }
        }

        return $template;
    }

    protected function replaceVariablesForField(string $filedModel): string
    {
        foreach (SeoVariableEnum::cases() as $variable) {
            if (str_contains($filedModel, $variable->value)) {
                $filedModel = str_ireplace(
                    search: $variable->value,
                    replace: $this->variableValue->match($variable),
                    subject: $filedModel
                );
            }
        }

        return $filedModel;
    }
}
