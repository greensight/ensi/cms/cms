<?php

namespace App\Domain\Seo\Actions\SeoVariables;

use App\Domain\Seo\Actions\SeoVariables\LoadVariables\Data\SeoVariablePayloadData;
use App\Domain\Seo\Actions\SeoVariables\LoadVariables\LoadVariableFactory;
use App\Domain\Seo\Models\SeoTemplate;

class FillSeoVariablesAction
{
    public function __construct(
        protected LoadVariableFactory $factory,
        protected SetSeoVariablesAction $setSeoVariablesAction,
    ) {
    }

    public function execute(SeoTemplate $template, SeoVariablePayloadData $payloadData): SeoTemplate
    {
        $loadVariableAction = $this->factory->fromModel($template, $payloadData);

        return $this->setSeoVariablesAction->execute($template, $loadVariableAction);
    }
}
