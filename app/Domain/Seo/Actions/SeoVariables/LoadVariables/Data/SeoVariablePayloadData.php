<?php

namespace App\Domain\Seo\Actions\SeoVariables\LoadVariables\Data;

class SeoVariablePayloadData
{
    public function __construct(public ?int $productId = null)
    {
    }
}
