<?php

namespace App\Domain\Seo\Actions\SeoVariables\LoadVariables\Data;

use App\Domain\Seo\Contracts\VariableValueData;
use App\Http\ApiV1\OpenApiGenerated\Enums\SeoVariableEnum;
use Ensi\CatalogCacheClient\Dto\ElasticOffer;

class ProductVariableValueData implements VariableValueData
{
    public function __construct(protected ElasticOffer $offer)
    {
    }

    public function match(SeoVariableEnum $enum): string
    {
        return match ($enum) {
            SeoVariableEnum::PRICE => (string)$this->offer->getPrice(),
            SeoVariableEnum::PRODUCT_NAME => $this->offer->getName(),
        };
    }
}
