<?php

namespace App\Domain\Seo\Actions\SeoVariables\LoadVariables;

use App\Domain\Seo\Actions\SeoVariables\LoadVariables\Data\SeoVariablePayloadData;
use App\Domain\Seo\Contracts\LoadLazyVariableAction;
use App\Domain\Seo\Models\SeoTemplate;
use App\Http\ApiV1\OpenApiGenerated\Enums\SeoTemplateTypeEnum;

class LoadVariableFactory
{
    public function __construct(protected LoadProductVariableAction $loadProductVariableAction)
    {
    }

    public function fromModel(SeoTemplate $template, SeoVariablePayloadData $payloadData): LoadLazyVariableAction
    {
        $action = match ($template->type) {
            SeoTemplateTypeEnum::PRODUCT,
            SeoTemplateTypeEnum::PRODUCT_DEFAULT => $this->loadProductVariableAction,
        };

        return $action->fillPayloadData($payloadData);
    }
}
