<?php

namespace App\Domain\Seo\Actions\SeoVariables\LoadVariables;

use App\Domain\Seo\Actions\SeoVariables\LoadVariables\Data\ProductVariableValueData;
use App\Domain\Seo\Actions\SeoVariables\LoadVariables\Data\SeoVariablePayloadData;
use App\Domain\Seo\Contracts\LoadLazyVariableAction;
use App\Domain\Seo\Contracts\VariableValueData;
use App\Exceptions\ValidateException;
use Ensi\CatalogCacheClient\Api\ElasticOffersApi;
use Ensi\CatalogCacheClient\Dto\SearchOneElasticOffersRequest;

class LoadProductVariableAction implements LoadLazyVariableAction
{
    protected ?int $productId = null;

    public function __construct(private readonly ElasticOffersApi $offersApi)
    {
    }

    public function fillPayloadData(SeoVariablePayloadData $payloadData): static
    {
        $this->productId = $payloadData->productId;

        return $this;
    }

    /**
     * @throws ValidateException
     */
    public function load(): VariableValueData
    {
        if (!$this->productId) {
            throw new ValidateException(
                "Для заполнения данных шаблона с типами PRODUCT и PRODUCT_DEFAULT необходимо передать id продукта"
            );
        }

        $request = new SearchOneElasticOffersRequest();
        $request->setFilter((object)['product_id' => $this->productId]);

        $offer = $this->offersApi->searchOneElasticOffer($request)->getData();

        return new ProductVariableValueData($offer);
    }
}
