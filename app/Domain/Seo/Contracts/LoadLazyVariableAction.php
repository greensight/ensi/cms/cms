<?php

namespace App\Domain\Seo\Contracts;

use App\Domain\Seo\Actions\SeoVariables\LoadVariables\Data\SeoVariablePayloadData;
use App\Exceptions\ValidateException;

interface LoadLazyVariableAction
{
    /**
     * Установка параметров необходимых для загрузки данных в seo-переменные
     */
    public function fillPayloadData(SeoVariablePayloadData $payloadData): static;

    /**
     * @throws ValidateException
     */
    public function load(): VariableValueData;
}
