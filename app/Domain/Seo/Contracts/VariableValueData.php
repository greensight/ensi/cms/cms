<?php

namespace App\Domain\Seo\Contracts;

use App\Http\ApiV1\OpenApiGenerated\Enums\SeoVariableEnum;

interface VariableValueData
{
    public function match(SeoVariableEnum $enum): string;
}
