<?php

namespace App\Domain\Seo\Models\Tests\Factories;

use App\Domain\Seo\Models\SeoTemplate;
use App\Domain\Seo\Models\SeoTemplateProduct;
use Ensi\LaravelTestFactories\BaseModelFactory;

class SeoTemplateProductFactory extends BaseModelFactory
{
    protected $model = SeoTemplateProduct::class;

    public function definition(): array
    {
        return [
            'template_id' => $this->templateId ?? SeoTemplate::factory(),
            'product_id' => $this->faker->modelId(),
        ];
    }

    public function withTemplate(SeoTemplate $template): self
    {
        return $this->state(['template_id' => $template->id]);
    }
}
