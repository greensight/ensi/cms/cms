<?php

namespace App\Domain\Seo\Models\Tests\Factories;

use App\Domain\Seo\Models\SeoTemplate;
use App\Http\ApiV1\OpenApiGenerated\Enums\SeoTemplateTypeEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class SeoTemplateFactory extends BaseModelFactory
{
    protected $model = SeoTemplate::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->unique()->word(),
            'type' => $this->faker->randomEnum(SeoTemplateTypeEnum::cases()),
            'header' => $this->faker->text(),
            'title' => $this->faker->nullable()->text(),
            'description' => $this->faker->nullable()->text(),
            'seo_text' => $this->faker->nullable()->text(),
            'is_active' => $this->faker->boolean(),
        ];
    }

    public function defaultProduct(): self
    {
        return $this->state([
            'is_active' => true,
            'type' => SeoTemplateTypeEnum::PRODUCT_DEFAULT,
        ]);
    }

    public function product(): self
    {
        return $this->state([
            'is_active' => true,
            'type' => SeoTemplateTypeEnum::PRODUCT,
        ]);
    }
}
