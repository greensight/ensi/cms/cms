<?php

namespace App\Domain\Seo\Models;

use App\Domain\Seo\Models\Tests\Factories\SeoTemplateFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\SeoTemplateTypeEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 *
 * @property string $name - название шаблона
 * @property SeoTemplateTypeEnum $type - тип шаблона
 * @property string $header - заголовок h1
 * @property string|null $title - заголовок окна браузера
 * @property string|null $description - описание страницы, мета-тег description
 * @property string|null $seo_text - SEO-текст
 * @property bool $is_active - активность шаблона
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read Collection<SeoTemplateProduct> $products
 */
class SeoTemplate extends Model
{
    protected $table = 'seo_templates';

    protected $fillable = [
        'name',
        'type',
        'header',
        'title',
        'description',
        'seo_text',
        'is_active',
    ];

    protected $casts = [
        'type' => SeoTemplateTypeEnum::class,
        'is_active' => 'bool',
    ];

    /** Список полей поддерживаемых использование SEO-переменных */
    public static function supportSeoVariable(): array
    {
        return ['header', 'title', 'description', 'seo_text'];
    }

    public function isDefaultProduct(): bool
    {
        return $this->is_active && $this->type == SeoTemplateTypeEnum::PRODUCT_DEFAULT;
    }

    public function deactivate(): void
    {
        $this->is_active = false;
        $this->save();
    }

    public function products(): HasMany
    {
        return $this->hasMany(SeoTemplateProduct::class, 'template_id');
    }

    public static function factory(): SeoTemplateFactory
    {
        return SeoTemplateFactory::new();
    }
}
