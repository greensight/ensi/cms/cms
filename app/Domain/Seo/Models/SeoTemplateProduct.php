<?php

namespace App\Domain\Seo\Models;

use App\Domain\Seo\Models\Tests\Factories\SeoTemplateProductFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $template_id - id шаблона
 * @property int $product_id - id товара
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read SeoTemplate $template
 */
class SeoTemplateProduct extends Model
{
    protected $table = 'seo_template_products';

    protected $fillable = ['template_id', 'product_id'];

    public function template(): BelongsTo
    {
        return $this->belongsTo(SeoTemplate::class);
    }

    public static function factory(): SeoTemplateProductFactory
    {
        return SeoTemplateProductFactory::new();
    }
}
