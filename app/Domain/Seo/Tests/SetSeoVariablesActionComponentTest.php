<?php

use App\Domain\Seo\Actions\SeoVariables\FillSeoVariablesAction;
use App\Domain\Seo\Actions\SeoVariables\LoadVariables\Data\SeoVariablePayloadData;
use App\Domain\Seo\Models\SeoTemplate;
use App\Domain\Support\Tests\Factories\CatalogCache\ElasticOfferFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\SeoVariableEnum;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Testing\Assert;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'variables');

test('Model SeoTemplate 200 check field', function () {
    $expectValue = 'string';
    foreach (SeoTemplate::supportSeoVariable() as $field) {
        $seoTemplate = SeoTemplate::factory()->create([$field => $expectValue]);
        Assert::assertEquals($expectValue, $seoTemplate->getAttribute($field));
    }
});

test('Action SetSeoVariablesAction replace different variable', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $field = current(SeoTemplate::supportSeoVariable());

    $productId = 1;
    $productName = 'name';
    $productPrice = 5000;

    $nameTemplate = SeoVariableEnum::PRODUCT_NAME->value;
    $priceTemplate = SeoVariableEnum::PRICE->value;

    $fieldTemplate = "name:$nameTemplate,:price:$priceTemplate";
    $expectFieldValue = "name:$productName,:price:$productPrice";

    $seoTemplate = SeoTemplate::factory()->create([$field => $fieldTemplate]);

    $this->mockCatalogCacheOffersApi()->allows([
        'searchOneElasticOffer' => ElasticOfferFactory::new()->makeResponse([
            'price' => $productPrice,
            'name' => $productName,
        ]),
    ]);

    $action = resolve(FillSeoVariablesAction::class);
    $totalSeoTemplate = $action->execute($seoTemplate, new SeoVariablePayloadData(productId: $productId));

    Assert::assertEquals($expectFieldValue, $totalSeoTemplate->getAttribute($field));
})->with(FakerProvider::$optionalDataset);

test('Action SetSeoVariablesAction replace all includes with variable', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $field = current(SeoTemplate::supportSeoVariable());

    $productId = 1;
    $productName = 'name';

    $nameTemplate = SeoVariableEnum::PRODUCT_NAME->value;

    $fieldTemplate = "name:$nameTemplate,:name:$nameTemplate";
    $expectFieldValue = "name:$productName,:name:$productName";

    $seoTemplate = SeoTemplate::factory()->create([$field => $fieldTemplate]);

    $this->mockCatalogCacheOffersApi()->allows([
        'searchOneElasticOffer' => ElasticOfferFactory::new()->makeResponse([
            'name' => $productName,
        ]),
    ]);

    $action = resolve(FillSeoVariablesAction::class);
    $totalSeoTemplate = $action->execute($seoTemplate, new SeoVariablePayloadData(productId: $productId));

    Assert::assertEquals($expectFieldValue, $totalSeoTemplate->getAttribute($field));
})->with(FakerProvider::$optionalDataset);
