<?php

namespace App\Domain\Support\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\ElasticOffer;
use Ensi\CatalogCacheClient\Dto\ElasticOfferResponse;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersResponse;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\File;
use Ensi\PimClient\Dto\ProductTypeEnum;

class ElasticOfferFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $hasGluing = $this->faker->boolean;

        return [
            'id' => $this->faker->modelId(),

            'product_id' => $this->faker->modelId(),
            'allow_publish' => $this->faker->boolean(),

            'main_image_file' => new File(EnsiFile::factory()->make()),

            'category_id' => $this->faker->modelId(),
            'brand_id' => $this->faker->nullable()->modelId(),

            'name' => $this->faker->sentence(3),
            'code' => $this->faker->slug(),
            'description' => $this->faker->nullable()->text(50),
            'type' => $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues()),
            'vendor_code' => $this->faker->nullable()->numerify('######'),
            'barcode' => $this->faker->nullable()->ean13(),

            'weight' => $this->faker->nullable()->randomFloat(4),
            'weight_gross' => $this->faker->nullable()->randomFloat(4),
            'length' => $this->faker->nullable()->randomNumber(),
            'width' => $this->faker->nullable()->randomNumber(),
            'height' => $this->faker->nullable()->randomNumber(),
            'is_adult' => $this->faker->boolean(),

            'price' => $this->faker->nullable()->randomNumber(),

            'gluing_name' => $hasGluing ? $this->faker->sentence() : null,
            'gluing_is_main' => $hasGluing ? $this->faker->boolean : null,
            'gluing_is_active' => $hasGluing ? $this->faker->boolean : null,
        ];
    }

    public function make(array $extra = []): ElasticOffer
    {
        return new ElasticOffer($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ElasticOfferResponse
    {
        return new ElasticOfferResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchElasticOffersResponse
    {
        return $this->generateResponseSearch(SearchElasticOffersResponse::class, $extras, $count, $pagination);
    }
}
