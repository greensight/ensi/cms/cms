<?php

namespace App\Domain\Support\Actions;

use App\Exceptions\ValidateException;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\ApiException as PimApiException;
use Ensi\PimClient\Dto\SearchProductsRequest;

class ValidateProductsExistAction
{
    public function __construct(protected ProductsApi $productsApi)
    {
    }

    public function execute(array $productIds): void
    {
        try {
            $request = new SearchProductsRequest();
            $request->setFilter((object)['id' => $productIds]);

            $products = $this->productsApi->searchProducts($request)->getData();
        } catch (PimApiException $e) {
            throw new ValidateException("Error occurred while fetching products, error code {$e->getCode()}");
        }

        if (count($productIds) != count($products)) {
            throw new ValidateException('Request contains non-existing products');
        }
    }
}
