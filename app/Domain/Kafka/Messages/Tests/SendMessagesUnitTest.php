<?php

use App\Domain\Kafka\Actions\Send\SendNameplateEventAction;
use App\Domain\Kafka\Actions\Send\SendNameplateProductEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\NameplatePayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\NameplateProductPayload;
use App\Domain\Nameplates\Models\Nameplate;
use App\Domain\Nameplates\Models\NameplateProduct;

use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

test("generate NameplatePayload success", function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');

    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->create();
    $payload = new NameplatePayload($nameplate);

    assertEquals($payload->jsonSerialize(), $nameplate->attributesToArray());
});

test("generate NameplateProductLinkPayload success", function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldReceive('execute');

    /** @var NameplateProduct $nameplateProduct */
    $nameplateProduct = NameplateProduct::factory()->create();
    $payload = new NameplateProductPayload($nameplateProduct);

    assertEquals($payload->jsonSerialize(), $nameplateProduct->attributesToArray());
});
