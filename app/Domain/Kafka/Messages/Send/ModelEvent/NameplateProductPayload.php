<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Kafka\Messages\Send\Payload;
use App\Domain\Nameplates\Models\NameplateProduct;

class NameplateProductPayload extends Payload
{
    public function __construct(protected NameplateProduct $nameplateProduct)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->nameplateProduct->id,

            'product_id' => $this->nameplateProduct->product_id,
            'nameplate_id' => $this->nameplateProduct->nameplate_id,

            'created_at' => $this->nameplateProduct->created_at?->toJSON(),
            'updated_at' => $this->nameplateProduct->updated_at?->toJSON(),
        ];
    }
}
