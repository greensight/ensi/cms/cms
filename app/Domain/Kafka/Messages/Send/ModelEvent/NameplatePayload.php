<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Kafka\Messages\Send\Payload;
use App\Domain\Nameplates\Models\Nameplate;

class NameplatePayload extends Payload
{
    public function __construct(protected Nameplate $nameplate)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->nameplate->id,

            'name' => $this->nameplate->name,
            'code' => $this->nameplate->code,

            'background_color' => $this->nameplate->background_color,
            'text_color' => $this->nameplate->text_color,

            'is_active' => $this->nameplate->is_active,

            'created_at' => $this->nameplate->created_at?->toJSON(),
            'updated_at' => $this->nameplate->updated_at?->toJSON(),
        ];
    }
}
