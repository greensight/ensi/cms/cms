<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\NameplatePayload;
use App\Domain\Nameplates\Models\Nameplate;

class SendNameplateEventAction extends SendMessageAction
{
    public function execute(Nameplate $nameplate, string $event): void
    {
        $modelEvent = new ModelEventMessage($nameplate, new NameplatePayload($nameplate), $event, 'nameplates');
        $this->send($modelEvent);
    }
}
