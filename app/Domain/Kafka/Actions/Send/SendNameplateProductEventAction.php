<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\NameplateProductPayload;
use App\Domain\Nameplates\Models\NameplateProduct;

class SendNameplateProductEventAction extends SendMessageAction
{
    public function execute(NameplateProduct $nameplateProduct, string $event): void
    {
        $modelEvent = new ModelEventMessage($nameplateProduct, new NameplateProductPayload($nameplateProduct), $event, 'nameplate-product-links');
        $this->send($modelEvent);
    }
}
