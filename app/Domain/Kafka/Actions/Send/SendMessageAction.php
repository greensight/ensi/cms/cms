<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\KafkaMessage;
use Ensi\LaravelPhpRdKafkaProducer\HighLevelProducer;

abstract class SendMessageAction
{
    protected function send(KafkaMessage $message): void
    {
        (new HighLevelProducer($message->topicKey()))->sendOne(json_encode($message->toArray()));
    }
}
