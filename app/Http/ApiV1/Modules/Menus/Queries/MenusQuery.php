<?php

namespace App\Http\ApiV1\Modules\Menus\Queries;

use App\Domain\Contents\Models\Menu;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class MenusQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Menu::query());

        $this->allowedIncludes(['items']);

        $this->allowedSorts([
            'id',
            'code',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('code'),
        ]);

        $this->defaultSort('id');
    }
}
