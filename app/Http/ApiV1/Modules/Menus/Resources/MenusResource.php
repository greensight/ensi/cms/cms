<?php

namespace App\Http\ApiV1\Modules\Menus\Resources;

use App\Domain\Contents\Models\Menu;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Menu */
class MenusResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'items' => MenuItemsResource::collection($this->whenLoaded('items')),
            'items_tree' => MenuTreesResource::collection($this->when($this->relationLoaded('items'), $this->getItemsTree())),
        ];
    }
}
