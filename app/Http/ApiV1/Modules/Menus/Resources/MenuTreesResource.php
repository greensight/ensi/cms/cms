<?php

namespace App\Http\ApiV1\Modules\Menus\Resources;

use App\Domain\Contents\Models\MenuItem;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin MenuItem */
class MenuTreesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'name' => $this->name,
            'url' => $this->url,
            'active' => $this->active,
            'children' => self::collection($this->children),
        ];
    }
}
