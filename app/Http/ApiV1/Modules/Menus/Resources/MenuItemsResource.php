<?php

namespace App\Http\ApiV1\Modules\Menus\Resources;

use App\Domain\Contents\Models\MenuItem;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin MenuItem */
class MenuItemsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'url' => $this->url,
            'name' => $this->name,
            'menu_id' => $this->menu_id,
            '_lft' => $this->_lft,
            '_rgt' => $this->_rgt,
            'parent_id' => $this->parent_id,
            'active' => $this->active,
        ];
    }
}
