<?php

namespace App\Http\ApiV1\Modules\Menus\Controllers;

use App\Http\ApiV1\Modules\Menus\Queries\MenusQuery;
use App\Http\ApiV1\Modules\Menus\Resources\MenusResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class MenusController
{
    public function search(PageBuilderFactory $pageBuilderFactory, MenusQuery $query): Responsable
    {
        return MenusResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(MenusQuery $query): Responsable
    {
        return MenusResource::make($query->firstOrFail());
    }
}
