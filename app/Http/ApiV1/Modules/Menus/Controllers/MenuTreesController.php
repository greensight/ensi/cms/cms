<?php

namespace App\Http\ApiV1\Modules\Menus\Controllers;

use App\Domain\Contents\Actions\Menu\ReplaceMenuTreesAction;
use App\Http\ApiV1\Modules\Menus\Requests\UpdateMenuTreesRequest;
use App\Http\ApiV1\Modules\Menus\Resources\MenuTreesResource;
use Illuminate\Contracts\Support\Responsable;

class MenuTreesController
{
    public function update(int $id, UpdateMenuTreesRequest $request, ReplaceMenuTreesAction $action): Responsable
    {
        return MenuTreesResource::collection($action->execute($id, $request->validated()));
    }
}
