<?php

namespace App\Http\ApiV1\Modules\Pages\Requests;

use App\Domain\Contents\Models\Page;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreatePageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', Rule::unique(Page::class, 'name')],
            'slug' => ['required', 'string', Rule::unique(Page::class, 'slug')],
            'content' => ['required', 'string'],
            'is_active' => ['required', 'boolean'],
            'active_from' => ['nullable', 'string', 'date'],
            'active_to' => ['nullable', 'string', 'date', 'after_or_equal:active_from'],
        ];
    }
}
