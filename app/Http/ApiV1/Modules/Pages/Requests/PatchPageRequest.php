<?php

namespace App\Http\ApiV1\Modules\Pages\Requests;

use App\Domain\Contents\Models\Page;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchPageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $id = $this->route('id');

        return [
            'name' => ['sometimes', 'required', 'string', Rule::unique(Page::class, 'name')->ignore($id)],
            'slug' => ['sometimes', 'required', 'string', Rule::unique(Page::class, 'slug')->ignore($id)],
            'content' => ['sometimes', 'required', 'string'],
            'is_active' => ['sometimes', 'boolean'],
            'active_from' => ['nullable', 'string', 'date'],
            'active_to' => ['nullable', 'string', 'date', 'after_or_equal:active_from'],
        ];
    }
}
