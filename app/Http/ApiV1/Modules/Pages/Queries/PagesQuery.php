<?php

namespace App\Http\ApiV1\Modules\Pages\Queries;

use App\Domain\Contents\Models\Page;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class PagesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Page::query());

        $this->allowedSorts([
            'id',
            'name',
            'is_active',
            'active_from',
            'active_to',
            'created_at',
            'updated_at',
        ]);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('slug'),
            AllowedFilter::exact('is_active'),

            ...StringFilter::make('name')->exact()->contain(),
            ...StringFilter::make('content')->contain(),

            AllowedFilter::scope('is_visible'),
        ]);
    }
}
