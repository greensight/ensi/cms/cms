<?php

namespace App\Http\ApiV1\Modules\Pages\Resources;

use App\Domain\Contents\Models\Page;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Page */
class PagesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'content' => $this->content,
            'is_active' => $this->is_active,
            'active_from' => $this->active_from,
            'active_to' => $this->active_to,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
