<?php

namespace App\Http\ApiV1\Modules\Pages\Controllers;

use App\Domain\Contents\Actions\Pages\CreatePageAction;
use App\Domain\Contents\Actions\Pages\DeletePageAction;
use App\Domain\Contents\Actions\Pages\PatchPageAction;
use App\Http\ApiV1\Modules\Pages\Queries\PagesQuery;
use App\Http\ApiV1\Modules\Pages\Requests\CreatePageRequest;
use App\Http\ApiV1\Modules\Pages\Requests\PatchPageRequest;
use App\Http\ApiV1\Modules\Pages\Resources\PagesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PagesController
{
    public function get(int $id, PagesQuery $query): PagesResource
    {
        return new PagesResource($query->findOrFail($id));
    }

    public function create(CreatePageRequest $request, CreatePageAction $action): PagesResource
    {
        return new PagesResource($action->execute($request->validated()));
    }

    public function patch(
        int $id,
        PatchPageRequest $request,
        PatchPageAction $action
    ): PagesResource {
        return new PagesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeletePageAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, PagesQuery $query): AnonymousResourceCollection
    {
        return PagesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(PagesQuery $query): PagesResource
    {
        return PagesResource::make($query->firstOrFail());
    }
}
