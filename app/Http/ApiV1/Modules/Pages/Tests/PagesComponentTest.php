<?php

use App\Domain\Contents\Models\Page;
use App\Http\ApiV1\Modules\Pages\Tests\Factories\PageRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/contents/pages:search 200', function () {
    Page::factory()->count(5)->create();

    /** @var Page $latest */
    $latest = Page::query()->latest('name')->first();

    $requestBody = [
        'sort' => ['-name'],
    ];

    postJson('/api/v1/contents/pages:search', $requestBody)
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $latest->id);
});

test('POST /api/v1/contents/pages:search 200 only active', function () {
    Page::factory()->active()->count(5)->create();
    Page::factory()->inactive()->count(10)->create();

    $requestBody = [
        'filter' => [
            'is_visible' => true,
        ],
    ];

    postJson('/api/v1/contents/pages:search', $requestBody)
        ->assertStatus(200)
        ->assertJsonCount(5, 'data');
});

test('POST /api/v1/contents/pages:search 200 active with null dates', function () {
    Page::factory()->count(10)->create([
        'is_active' => true,
        'active_from' => null,
        'active_to' => null,
    ]);

    Page::factory()->inactive()->count(5)->create();

    $requestBody = [
        'filter' => [
            'is_visible' => true,
        ],
    ];

    postJson('/api/v1/contents/pages:search', $requestBody)
        ->assertStatus(200)
        ->assertJsonCount(10, 'data');
});

test('POST /api/v1/contents/pages:search 200 only inactive', function () {
    Page::factory()->active()->count(5)->create();
    Page::factory()->inactive()->count(10)->create();

    $requestBody = [
        'filter' => [
            'is_visible' => false,
        ],
    ];

    postJson('/api/v1/contents/pages:search', $requestBody)
        ->assertStatus(200)
        ->assertJsonCount(10, 'data');
});

test('POST /api/v1/contents/pages:search 400 incorrect filter', function () {
    Page::factory()->count(5)->create();

    $requestBody = [
        'filter' => [
            'incorrect_filter' => 'value',
        ],
    ];

    postJson('/api/v1/contents/pages:search', $requestBody)
        ->assertStatus(400)
        ->assertJsonStructure([
            'data',
            'errors',
        ]);
});

test('POST /api/v1/contents/pages:search 200 empty response', function () {
    Page::factory()->count(5)->create();

    $requestBody = [
        'filter' => [
            'name_like' => 'Missing name',
        ],
    ];

    postJson('/api/v1/contents/pages:search', $requestBody)
        ->assertStatus(200)
        ->assertJsonPath('data', []);
});

test('POST /api/v1/contents/pages:search-one 200', function () {
    /** @var Page $latest */
    $latest = Page::factory()->count(5)->create()->last();

    $requestBody = [
        'filter' => [
            'id' => $latest->id,
        ],
    ];

    postJson('/api/v1/contents/pages:search-one', $requestBody)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $latest->id);
});

test('POST /api/v1/contents/pages 200 create page', function () {
    $pageData = PageRequestFactory::new()->make();

    postJson('/api/v1/contents/pages', $pageData)
        ->assertCreated()
        ->assertJsonPath('data.name', $pageData['name'])
        ->assertJsonPath('data.slug', $pageData['slug'])
        ->assertJsonPath('data.content', $pageData['content'])
        ->assertJsonPath('data.is_active', $pageData['is_active']);

    assertDatabaseHas((new Page())->getTable(), [
        'name' => $pageData['name'],
        'slug' => $pageData['slug'],
        'content' => $pageData['content'],
        'is_active' => $pageData['is_active'],
    ]);
});

test('GET /api/v1/contents/pages/{id} 200', function () {
    /** @var Page $page */
    $page = Page::factory()->create();

    getJson("/api/v1/contents/pages/$page->id")
        ->assertStatus(200)
        ->assertJsonPath('data.name', $page->name)
        ->assertJsonPath('data.slug', $page->slug)
        ->assertJsonPath('data.content', $page->content)
        ->assertJsonPath('data.is_active', $page->is_active);
});

test('GET /api/v1/contents/pages/{id} 404', function () {
    getJson("/api/v1/contents/pages/123")
        ->assertStatus(404);
});

test('PATCH /api/v1/contents/pages/{id} 200', function () {
    /** @var Page $page */
    $page = Page::factory()->create();

    $pageData = PageRequestFactory::new()->make();

    patchJson("/api/v1/contents/pages/$page->id", $pageData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $page->id)
        ->assertJsonPath('data.name', $pageData['name'])
        ->assertJsonPath('data.slug', $pageData['slug'])
        ->assertJsonPath('data.content', $pageData['content'])
        ->assertJsonPath('data.is_active', $pageData['is_active']);
});

test('DELETE /api/v1/contents/pages/{id} 200', function () {
    /** @var Page $page */
    $page = Page::factory()->create();

    deleteJson("/api/v1/contents/pages/$page->id")
        ->assertStatus(200);

    assertDatabaseMissing((new Page())->getTable(), [
        'id' => $page->id,
    ]);
});
