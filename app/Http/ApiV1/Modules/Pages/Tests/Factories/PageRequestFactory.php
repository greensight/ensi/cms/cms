<?php

namespace App\Http\ApiV1\Modules\Pages\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Illuminate\Support\Carbon;

class PageRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->words(3, true),
            'slug' => $this->faker->slug(),
            'content' => $this->faker->randomHtml(),
            'is_active' => $this->faker->boolean(),
            'active_from' => Carbon::today(),
            'active_to' => Carbon::tomorrow(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
