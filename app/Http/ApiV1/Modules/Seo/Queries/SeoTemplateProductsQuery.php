<?php

namespace App\Http\ApiV1\Modules\Seo\Queries;

use App\Domain\Seo\Models\SeoTemplateProduct;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class SeoTemplateProductsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(SeoTemplateProduct::query());

        $this->allowedSorts(['id', 'template_id', 'product_id', 'created_at', 'updated_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('template_id'),
            AllowedFilter::exact('product_id'),
            ...DateFilter::make('created_at')->gte()->lte(),
            ...DateFilter::make('updated_at')->gte()->lte(),
        ]);

        $this->defaultSort('id');
    }
}
