<?php

namespace App\Http\ApiV1\Modules\Seo\Queries;

use App\Domain\Seo\Models\SeoTemplate;
use App\Http\ApiV1\Modules\Seo\Filters\TemplateProductFilter;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\ExtraFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class SeoTemplatesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(SeoTemplate::query());

        $this->allowedSorts(['id', 'name', 'type', 'is_active', 'created_at', 'updated_at']);

        $this->allowedIncludes(['products']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            ...StringFilter::make('name')->contain(),
            AllowedFilter::exact('type'),
            ...StringFilter::make('header')->contain(),
            AllowedFilter::exact('is_active'),

            ...DateFilter::make('created_at')->gte()->lte(),
            ...DateFilter::make('updated_at')->gte()->lte(),

            ...ExtraFilter::nested('products', [
                AllowedFilter::exact('product_id'),
            ]),

            ...$this->getCustomTemplateFilter(),
        ]);

        $this->defaultSort('id');
    }

    /**
     * Кастомные фильтры предназначение для получения заполненного шаблона c префиксом template_
     */
    protected function getCustomTemplateFilter(): array
    {
        return [
            AllowedFilter::custom('template_product_id', new TemplateProductFilter()),
        ];
    }
}
