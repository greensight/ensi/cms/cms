<?php

namespace App\Http\ApiV1\Modules\Seo\Requests;

use App\Domain\Seo\Models\SeoTemplateProduct;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class AddSeoTemplateProductsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $templateId = (int)$this->route('id');

        return [
            'ids' => ['required', 'array', 'min:1'],
            'ids.*' => ['integer', 'distinct',
                Rule::unique(SeoTemplateProduct::class, 'product_id')
                    ->where('template_id', $templateId),
            ],
        ];
    }

    public function getProductIds(): array
    {
        return $this->validated('ids');
    }
}
