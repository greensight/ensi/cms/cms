<?php

namespace App\Http\ApiV1\Modules\Seo\Requests;

use App\Domain\Seo\Models\SeoTemplate;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchSeoTemplateRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $id = (int) $this->route('id');

        return [
            'is_active' => ['boolean'],
            'seo_text' => ['nullable', 'string'],
            'description' => ['nullable', 'string'],
            'title' => ['nullable', 'string'],
            'header' => ['string'],
            'type' => ['integer'],
            'name' => ['string', Rule::unique(SeoTemplate::class)->ignore($id)],
        ];
    }
}
