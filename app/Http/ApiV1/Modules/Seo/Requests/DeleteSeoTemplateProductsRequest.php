<?php

namespace App\Http\ApiV1\Modules\Seo\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteSeoTemplateProductsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'ids' => ['required', 'array', 'min:1'],
            'ids.*' => ['integer', 'distinct'],
        ];
    }

    public function getProductIds(): array
    {
        return $this->input('ids');
    }
}
