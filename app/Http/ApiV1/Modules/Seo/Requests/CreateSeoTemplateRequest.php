<?php

namespace App\Http\ApiV1\Modules\Seo\Requests;

use App\Domain\Seo\Models\SeoTemplate;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateSeoTemplateRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'is_active' => ['required', 'boolean'],
            'seo_text' => ['nullable', 'string'],
            'description' => ['nullable', 'string'],
            'title' => ['nullable', 'string'],
            'header' => ['required', 'string'],
            'type' => ['required', 'integer'],
            'name' => ['required', 'string', Rule::unique(SeoTemplate::class)],
        ];
    }
}
