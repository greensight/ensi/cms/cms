<?php

namespace App\Http\ApiV1\Modules\Seo\Requests;

use App\Domain\Seo\Actions\SeoVariables\LoadVariables\Data\SeoVariablePayloadData;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class SeoVariablesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'seo_variables' => ['array'],
            'seo_variables.fill' => ['boolean'],
            'seo_variables.payload' => ['array'],
            'seo_variables.payload.product_id' => ['integer'],
        ];
    }

    public function isFill(): bool
    {
        return $this->input('seo_variables.fill', false);
    }

    public function convertToObject(): SeoVariablePayloadData
    {
        $productId = $this->input('seo_variables.payload.product_id');

        return new SeoVariablePayloadData(
            productId: $productId,
        );
    }
}
