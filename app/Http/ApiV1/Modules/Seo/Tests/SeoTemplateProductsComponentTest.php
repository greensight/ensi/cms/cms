<?php

use App\Domain\Seo\Models\SeoTemplate;
use App\Domain\Seo\Models\SeoTemplateProduct;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Collection;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/seo/template-products/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var SeoTemplateProduct $seoTemplateProduct */
    $seoTemplateProduct = SeoTemplateProduct::factory()->create();

    getJson("/api/v1/seo/template-products/{$seoTemplateProduct->id}")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $seoTemplateProduct->id);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/seo/template-products/{id} 404', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    getJson('/api/v1/seo/template-products/404')
        ->assertStatus(404);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/seo/template-products:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var SeoTemplate $seoTemplate1 */
    $seoTemplate1 = SeoTemplate::factory()->create();
    /** @var SeoTemplate $seoTemplate2 */
    $seoTemplate2 = SeoTemplate::factory()->create();

    /** @var Collection<SeoTemplateProduct> $seoTemplateProducts */
    $seoTemplateProducts = SeoTemplateProduct::factory()
        ->count(10)
        ->sequence(
            ['template_id' => $seoTemplate1->id],
            ['template_id' => $seoTemplate2->id],
        )
        ->create();

    postJson('/api/v1/seo/template-products:search', [
        "filter" => ["template_id" => $seoTemplate2->id],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $seoTemplateProducts->last()->id)
        ->assertJsonPath('data.0.template_id', $seoTemplate2->id);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/seo/template-products:search filter success", function (
    ?bool $always,
    string $fieldKey,
    $value = null,
    ?string $filterKey = null,
    $filterValue = null
) {
    FakerProvider::$optionalAlways = $always;

    /** @var SeoTemplateProduct $seoTemplateProduct */
    $seoTemplateProduct = SeoTemplateProduct::factory()->create($value ? [$fieldKey => $value] : []);
    SeoTemplateProduct::factory()->create();

    postJson("/api/v1/seo/template-products:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $seoTemplateProduct->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $seoTemplateProduct->id);
})->with(FakerProvider::$optionalDataset, [
    ['id'],
    ['template_id'],
    ['product_id', 1],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/seo/template-products:search sort success", function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    SeoTemplateProduct::factory()->create();
    postJson("/api/v1/seo/template-products:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id', 'template_id', 'product_id', 'created_at', 'updated_at',
], FakerProvider::$optionalDataset);
