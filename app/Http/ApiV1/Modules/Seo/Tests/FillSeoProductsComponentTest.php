<?php

use App\Domain\Seo\Models\SeoTemplate;
use App\Domain\Seo\Models\SeoTemplateProduct;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;
use function Pest\Laravel\travelTo;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/seo/products:search-one 200 default template', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productId = 1;
    /** @var SeoTemplate $defaultTemplate */
    $defaultTemplate = SeoTemplate::factory()->defaultProduct()->create();

    postJson('/api/v1/seo/templates:search-one', [
        "filter" => ["template_product_id" => $productId],
        "seo_variables" => [
            "fill" => true,
            "payload" => ["product_id" => $productId],
        ],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $defaultTemplate->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/seo/products:search-one 200 product template', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productId = 1;
    /** @var SeoTemplate $defaultTemplate */
    $defaultTemplate = SeoTemplate::factory()->defaultProduct()->create();
    /** @var SeoTemplate $productTemplate */
    $productTemplate = SeoTemplate::factory()->product()->create();
    SeoTemplateProduct::factory()->withTemplate($productTemplate)->create(['product_id' => $productId]);

    postJson('/api/v1/seo/templates:search-one', [
        "filter" => ["template_product_id" => $productId],
        "seo_variables" => [
            "fill" => true,
            "payload" => ["product_id" => $productId],
        ],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $productTemplate->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/seo/products:search-one 200 last template', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productId = 1;

    /** @var SeoTemplate $productTemplate */
    $productTemplate = SeoTemplate::factory()->defaultProduct()->create();
    SeoTemplateProduct::factory()->withTemplate($productTemplate)->create(['product_id' => $productId]);

    /** @var SeoTemplate $lastProductTemplate */
    $lastProductTemplate = travelTo(now()->addDay(), fn () => SeoTemplate::factory()->product()->create());
    SeoTemplateProduct::factory()->withTemplate($lastProductTemplate)->create(['product_id' => $productId]);

    // проверка, что не отдаем шаблон по умолчанию, если он последний
    /** @var SeoTemplate $defaultTemplate */
    $defaultTemplate = travelTo(now()->addDays(2), fn () => SeoTemplate::factory()->defaultProduct()->create());

    postJson('/api/v1/seo/templates:search-one', [
        "filter" => ["template_product_id" => $productId],
        "seo_variables" => [
            "fill" => true,
            "payload" => ["product_id" => $productId],
        ],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $lastProductTemplate->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/seo/templates:search-one 404', function () {
    postJson('/api/v1/seo/templates:search-one')
        ->assertStatus(404);
});
