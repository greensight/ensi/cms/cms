<?php

use App\Domain\Seo\Models\SeoTemplate;
use App\Domain\Seo\Models\SeoTemplateProduct;
use App\Domain\Support\Tests\Factories\Pim\ProductFactory;
use App\Http\ApiV1\Modules\Seo\Tests\Factories\SeoTemplateRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\SeoTemplateTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use App\Http\ApiV1\Support\Tests\Factories\RelationIdsRequestFactory;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Collection;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/seo/templates 201', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    SeoTemplate::factory()->create();
    $request = SeoTemplateRequestFactory::new()->make();

    $id = postJson('/api/v1/seo/templates', $request)
        ->assertStatus(201)
        ->json('data.id');

    assertDatabaseHas(SeoTemplate::class, ['id' => $id]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/seo/templates default product template 201', function (bool $active, ?bool $always) {
    FakerProvider::$optionalAlways = $always;
    /** @var SeoTemplate $template */
    $template = SeoTemplate::factory()->defaultProduct()->create();

    $request = SeoTemplateRequestFactory::new()->defaultProduct()->make(['is_active' => $active]);

    $id = postJson('/api/v1/seo/templates', $request)
        ->assertStatus(201)
        ->json('data.id');

    assertDatabaseHas(SeoTemplate::class, ['id' => $id, 'is_active' => $active]);
    assertDatabaseHas(SeoTemplate::class, ['id' => $template->id, 'is_active' => !$active]);
})->with([
    'deactivate' => true,
    'not deactivate' => false,
], FakerProvider::$optionalDataset);

test('POST /api/v1/seo/templates 400', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $this->skipNextOpenApiRequestValidation();

    $request = SeoTemplateRequestFactory::new()->make(['name' => null]);

    postJson('/api/v1/seo/templates', $request)
        ->assertStatus(400);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/seo/templates/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var SeoTemplate $seoTemplate */
    $seoTemplate = SeoTemplate::factory()->create();

    $newName = $seoTemplate->name . "_new";
    $request = SeoTemplateRequestFactory::new()->make(['name' => $newName]);

    patchJson("/api/v1/seo/templates/{$seoTemplate->id}", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.name', $newName);

    assertDatabaseHas(SeoTemplate::class, ['id' => $seoTemplate->id, 'name' => $newName]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/seo/templates/{id} 400', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $this->skipNextOpenApiRequestValidation();

    $request = SeoTemplateRequestFactory::new()->make(['name' => null]);

    patchJson('/api/v1/seo/templates/400', $request)
        ->assertStatus(400);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/seo/templates/{id} 404', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    patchJson('/api/v1/seo/templates/404')
        ->assertStatus(404);
})->with(FakerProvider::$optionalDataset);

test('DELETE /api/v1/seo/templates/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var SeoTemplate $seoTemplate */
    $seoTemplate = SeoTemplate::factory()->create();

    deleteJson("/api/v1/seo/templates/{$seoTemplate->id}")
        ->assertStatus(200);

    assertModelMissing($seoTemplate);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/seo/templates/{id}:add-products 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $products = ProductFactory::new()->makeResponseSearch(count: 2);
    $productIds = collect($products->getData())->pluck('id')->toArray();

    $this->mockPimProductsApi()->allows([
        'searchProducts' => $products,
    ]);

    /** @var SeoTemplate $seoTemplate */
    $seoTemplate = SeoTemplate::factory()->create();

    $request = RelationIdsRequestFactory::new()->fillIds($productIds)->make();

    postJson("/api/v1/seo/templates/{$seoTemplate->id}:add-products", $request)
        ->assertStatus(200);

    foreach ($productIds as $productId) {
        assertDatabaseHas(SeoTemplateProduct::class, ['template_id' => $seoTemplate->id, 'product_id' => $productId]);
    }
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/seo/templates/{id}:add-products 400 already exists', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productId = 1;
    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseSearch(['id' => $productId]),
    ]);

    /** @var SeoTemplate $seoTemplate */
    $seoTemplate = SeoTemplate::factory()->create();
    SeoTemplateProduct::factory()->withTemplate($seoTemplate)->create(['product_id' => $productId]);

    $request = RelationIdsRequestFactory::new()->fillIds([$productId])->make();

    postJson("/api/v1/seo/templates/{$seoTemplate->id}:add-products", $request)
        ->assertStatus(400);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/seo/templates/{id}:add-products 400 product invalid', function () {
    $productId = 1;
    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseSearch(count: 0),
    ]);

    /** @var SeoTemplate $seoTemplate */
    $seoTemplate = SeoTemplate::factory()->create();

    $request = RelationIdsRequestFactory::new()->fillIds([$productId])->make();

    postJson("/api/v1/seo/templates/{$seoTemplate->id}:add-products", $request)
        ->assertStatus(400);
});

test('POST /api/v1/seo/templates/{id}:add-products 404', function () {
    $request = RelationIdsRequestFactory::new()->make();

    postJson("/api/v1/seo/templates/404:add-products", $request)
        ->assertStatus(404);
});

test('DELETE /api/v1/seo/templates/{id}:delete-products 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var SeoTemplate $seoTemplate */
    $seoTemplate = SeoTemplate::factory()->create();
    $productIds = [2, 3];

    $templateProductFactory = SeoTemplateProduct::factory()->withTemplate($seoTemplate);

    $seoTemplateProducts = collect($productIds)
        ->map(fn (int $productId) => $templateProductFactory->create(['product_id' => $productId]));

    /** @var SeoTemplate $seoTemplateAnother */
    $seoTemplateAnother = SeoTemplate::factory()->create();
    /** @var SeoTemplateProduct $seoTemplateProductAnother */
    $seoTemplateProductAnother = SeoTemplateProduct::factory()->withTemplate($seoTemplateAnother)->create();

    $request = RelationIdsRequestFactory::new()->fillIds($productIds)->make();

    deleteJson("/api/v1/seo/templates/{$seoTemplate->id}:delete-products", $request)
        ->assertStatus(200);

    assertModelExists($seoTemplateProductAnother);

    $seoTemplateProducts->each(fn (SeoTemplateProduct $templateProduct) => assertModelMissing($templateProduct));
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/seo/templates/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var SeoTemplate $seoTemplate */
    $seoTemplate = SeoTemplate::factory()->create();

    getJson("/api/v1/seo/templates/{$seoTemplate->id}")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $seoTemplate->id);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/seo/templates/{id} 404', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    getJson('/api/v1/seo/templates/404')
        ->assertStatus(404);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/seo/templates:search-one 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Collection<SeoTemplate> $templates */
    $templates = SeoTemplate::factory()
        ->forEachSequence(
            ['is_active' => true],
            ['is_active' => false],
        )
        ->create();

    postJson('/api/v1/seo/templates:search-one', [
        "filter" => ["is_active" => false],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $templates->last()->id)
        ->assertJsonPath('data.is_active', false);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/seo/templates:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Collection<SeoTemplate> $templates */
    $templates = SeoTemplate::factory()
        ->count(10)
        ->sequence(
            ['is_active' => true],
            ['is_active' => false],
        )
        ->create();

    postJson('/api/v1/seo/templates:search', [
        "filter" => ["is_active" => false],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $templates->last()->id)
        ->assertJsonPath('data.0.is_active', false);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/seo/templates:search product_id filter success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var SeoTemplateProduct $templateProduct */
    $templateProduct = SeoTemplateProduct::factory()->create();

    /** @var SeoTemplateProduct $templateProductAnother */
    $templateProductAnother = SeoTemplateProduct::factory()->create();

    postJson("/api/v1/seo/templates:search", ["filter" => ['product_id' => $templateProduct->product_id]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $templateProduct->template->id);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/seo/templates:search filter success", function (
    ?bool $always,
    string $fieldKey,
    $value = null,
    ?string $filterKey = null,
    $filterValue = null
) {
    FakerProvider::$optionalAlways = $always;

    /** @var SeoTemplate $template */
    $template = SeoTemplate::factory()->create($value ? [$fieldKey => $value] : []);
    SeoTemplate::factory()->create();

    postJson("/api/v1/seo/templates:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $template->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $template->id);
})->with(FakerProvider::$optionalDataset, [
    ['id'],
    ['name', 'Эксклюзивно в "Ensi"!', 'name_like', 'ensi'],
    ['type', SeoTemplateTypeEnum::PRODUCT, 'type', [SeoTemplateTypeEnum::PRODUCT, SeoTemplateTypeEnum::PRODUCT_DEFAULT]],
    ['is_active', true, 'is_active', true],
    ['header', 'Эксклюзивно в "Ensi"!', 'header_like', 'ensi'],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/seo/templates:search sort success", function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    SeoTemplate::factory()->create();
    postJson("/api/v1/seo/templates:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id', 'name', 'type', 'is_active', 'created_at', 'updated_at',
], FakerProvider::$optionalDataset);

test("POST /api/v1/seo/templates:search include success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var SeoTemplate $template */
    $template = SeoTemplate::factory()->create();
    /** @var SeoTemplateProduct $templateProduct */
    $templateProduct = SeoTemplateProduct::factory()->withTemplate($template)->create();

    postJson("/api/v1/seo/templates:search", ["include" => ['products']])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data.0.products')
        ->assertJsonPath('data.0.products.0.id', $templateProduct->id);
})->with(FakerProvider::$optionalDataset);
