<?php

namespace App\Http\ApiV1\Modules\Seo\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\SeoTemplateTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class SeoTemplateRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->unique()->word(),
            'type' => $this->faker->randomEnum(SeoTemplateTypeEnum::cases()),
            'header' => $this->faker->text(),
            'title' => $this->faker->nullable()->text(),
            'description' => $this->faker->nullable()->text(),
            'seo_text' => $this->faker->nullable()->text(),
            'is_active' => $this->faker->boolean(),
        ];
    }

    public function defaultProduct(): self
    {
        return $this->state([
            'is_active' => true,
            'type' => SeoTemplateTypeEnum::PRODUCT_DEFAULT,
        ]);
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
