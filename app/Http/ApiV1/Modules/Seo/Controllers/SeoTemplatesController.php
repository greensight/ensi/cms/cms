<?php

namespace App\Http\ApiV1\Modules\Seo\Controllers;

use App\Domain\Seo\Actions\SeoVariables\FillSeoVariablesAction;
use App\Domain\Seo\Actions\TemplateProducts\AddProductsAction;
use App\Domain\Seo\Actions\TemplateProducts\DeleteProductsAction;
use App\Domain\Seo\Actions\Templates\CreateSeoTemplateAction;
use App\Domain\Seo\Actions\Templates\DeleteSeoTemplateAction;
use App\Domain\Seo\Actions\Templates\PatchSeoTemplateAction;
use App\Domain\Seo\Models\SeoTemplate;
use App\Http\ApiV1\Modules\Seo\Queries\SeoTemplatesQuery;
use App\Http\ApiV1\Modules\Seo\Requests\AddSeoTemplateProductsRequest;
use App\Http\ApiV1\Modules\Seo\Requests\CreateSeoTemplateRequest;
use App\Http\ApiV1\Modules\Seo\Requests\DeleteSeoTemplateProductsRequest;
use App\Http\ApiV1\Modules\Seo\Requests\PatchSeoTemplateRequest;
use App\Http\ApiV1\Modules\Seo\Requests\SeoVariablesRequest;
use App\Http\ApiV1\Modules\Seo\Resources\SeoTemplatesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class SeoTemplatesController
{
    public function get(int $id, SeoTemplatesQuery $query): Responsable
    {
        return SeoTemplatesResource::make($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, SeoTemplatesQuery $query): Responsable
    {
        return SeoTemplatesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(SeoTemplatesQuery $query, SeoVariablesRequest $request, FillSeoVariablesAction $action): Responsable
    {
        /** @var SeoTemplate $template */
        $template = $query->firstOrFail();

        if ($request->isFill()) {
            $template = $action->execute($template, $request->convertToObject());
        }

        return SeoTemplatesResource::make($template);
    }

    public function create(CreateSeoTemplateRequest $request, CreateSeoTemplateAction $action): Responsable
    {
        return SeoTemplatesResource::make($action->execute($request->validated()));
    }

    public function patch(int $id, PatchSeoTemplateRequest $request, PatchSeoTemplateAction $action): Responsable
    {
        return SeoTemplatesResource::make($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteSeoTemplateAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function addProducts(int $id, SeoTemplatesQuery $query, AddSeoTemplateProductsRequest $request, AddProductsAction $action): Responsable
    {
        $action->execute($id, $query->getEloquentBuilder(), $request->getProductIds());

        return new EmptyResource();
    }

    public function deleteProducts(int $id, DeleteSeoTemplateProductsRequest $request, DeleteProductsAction $action): Responsable
    {
        $action->execute($id, $request->getProductIds());

        return new EmptyResource();
    }
}
