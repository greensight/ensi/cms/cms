<?php

namespace App\Http\ApiV1\Modules\Seo\Controllers;

use App\Http\ApiV1\Modules\Seo\Queries\SeoTemplateProductsQuery;
use App\Http\ApiV1\Modules\Seo\Resources\SeoTemplateProductsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class SeoTemplateProductsController
{
    public function get(int $id, SeoTemplateProductsQuery $query): Responsable
    {
        return SeoTemplateProductsResource::make($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, SeoTemplateProductsQuery $query): Responsable
    {
        return SeoTemplateProductsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
