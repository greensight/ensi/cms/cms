<?php

namespace App\Http\ApiV1\Modules\Seo\Resources;

use App\Domain\Seo\Models\SeoTemplateProduct;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin SeoTemplateProduct */
class SeoTemplateProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'template_id' => $this->template_id,
            'product_id' => $this->product_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
