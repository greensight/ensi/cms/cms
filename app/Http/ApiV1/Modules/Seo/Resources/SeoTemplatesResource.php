<?php

namespace App\Http\ApiV1\Modules\Seo\Resources;

use App\Domain\Seo\Models\SeoTemplate;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin SeoTemplate */
class SeoTemplatesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'header' => $this->header,
            'title' => $this->title,
            'description' => $this->description,
            'seo_text' => $this->seo_text,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'products' => SeoTemplateProductsResource::collection($this->whenLoaded('products')),
        ];
    }
}
