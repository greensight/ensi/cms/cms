<?php

namespace App\Http\ApiV1\Modules\Seo\Filters;

use App\Http\ApiV1\OpenApiGenerated\Enums\SeoTemplateTypeEnum;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class TemplateProductFilter implements Filter
{
    /**
     * Возвращает активные шаблоны по продукту в порядке актуальности + шаблон по умолчанию
     * @param int $value - идентификатор продукта
     */
    public function __invoke(Builder $query, $value, string $property): Builder
    {
        return $query->where('is_active', true)
            ->where(
                fn (Builder $query) => $query
                    ->where(
                        fn (Builder $query) => $query
                            ->where('type', SeoTemplateTypeEnum::PRODUCT)
                            ->whereHas(
                                'products',
                                fn (Builder $query) => $query->where('product_id', $value)
                            )
                    )->orWhere('type', SeoTemplateTypeEnum::PRODUCT_DEFAULT)
            )
            ->orderBy('type')
            ->orderByDesc('updated_at');
    }
}
