<?php

namespace App\Http\ApiV1\Modules\Banners\Controllers;

use App\Domain\Contents\Actions\Banners\CreateBannerAction;
use App\Domain\Contents\Actions\Banners\DeleteBannerAction;
use App\Domain\Contents\Actions\Banners\ReplaceBannerAction;
use App\Http\ApiV1\Modules\Banners\Queries\BannersQuery;
use App\Http\ApiV1\Modules\Banners\Requests\CreateBannerRequest;
use App\Http\ApiV1\Modules\Banners\Requests\ReplaceBannerRequest;
use App\Http\ApiV1\Modules\Banners\Resources\BannersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class BannersController
{
    public function create(CreateBannerRequest $request, CreateBannerAction $action): Responsable
    {
        return new BannersResource($action->execute($request->validated(), $request->getButton()));
    }

    public function replace(int $id, ReplaceBannerRequest $request, ReplaceBannerAction $action): Responsable
    {
        return new BannersResource($action->execute($id, $request->validated(), $request->getButton()));
    }

    public function delete(int $id, DeleteBannerAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, BannersQuery $query): Responsable
    {
        return BannersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(BannersQuery $query): Responsable
    {
        return BannersResource::make($query->withButton()->firstOrFail());
    }
}
