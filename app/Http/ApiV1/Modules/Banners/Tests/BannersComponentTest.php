<?php

use App\Domain\Contents\Models\Banner;
use App\Domain\Contents\Models\BannerButton;
use App\Http\ApiV1\Modules\Banners\Tests\Factories\BannerRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

test('POST /api/v1/contents/banners:search 200', function () {
    $banners = Banner::factory()
        ->count(10)
        ->sequence(
            ['is_active' => false],
            ['is_active' => true],
        )
        ->create();

    postJson('/api/v1/contents/banners:search', [
        "filter" => ["is_active" => true],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $banners->last()->id)
        ->assertJsonPath('data.0.is_active', true);
});

test("POST /api/v1/contents/banners:search filter success", function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var Banner $banner */
    $banner = Banner::factory()->create($value ? [$fieldKey => $value] : []);
    Banner::factory()->create();

    postJson("/api/v1/contents/banners:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $banner->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $banner->id);
})->with([
    ['code', 'banner-text'],
    ['name', 'banner text'],
    ['is_active', false],
    ['name', 'extra banner name', 'name_like', 'banner'],
    ['code', 'extra-banner-name', 'code_like', 'banner'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test("POST /api/v1/contents/banners:search sort success", function (string $sort) {
    Banner::factory()->create();
    postJson("/api/v1/contents/banners:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'name',
    'sort',
    'code',
    'is_active',
    'created_at',
    'updated_at',
]);

test('POST /api/v1/contents/banners:search-one 200', function () {
    $name = 'find me';
    $firstBanner = Banner::factory()->createOne(['name' => $name]);

    postJson('/api/v1/contents/banners:search-one', [
        "filter" => ["name" => $name],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $firstBanner->id);
});

test('POST /api/v1/contents/banners 200', function (bool $useSort) {
    $request = BannerRequestFactory::new()->withSort($useSort)->make();

    postJson('/api/v1/contents/banners', $request)
        ->assertStatus(201);

    assertDatabaseHas((new Banner())->getTable(), [
        'name' => $request['name'],
    ]);
})->with([
    [true],
    [false],
]);

test('POST /api/v1/contents/banners with button 200', function () {
    $request = BannerRequestFactory::new()->withButton()->make();

    $response = postJson('/api/v1/contents/banners', $request)
        ->assertStatus(201)
        ->assertJsonStructure(['data' => ['button' => ['id', 'url', 'text', 'location', 'type']]])
        ->assertJsonPath('data.button.text', $request['button']['text']);

    assertDatabaseHas((new Banner())->getTable(), [
        'name' => $request['name'],
        'button_id' => $response['data']['button_id'],
    ]);

    assertDatabaseHas((new BannerButton())->getTable(), [
        'id' => $response['data']['button_id'],
        'text' => $request['button']['text'],
    ]);
});

test('PUT /api/v1/contents/banners/{id} 200', function () {
    /** @var Banner $banner */
    $banner = Banner::factory()->create();

    $request = BannerRequestFactory::new()->make();

    putJson("/api/v1/contents/banners/{$banner->id}", $request)
        ->assertJsonPath('data.name', $request['name'])
        ->assertStatus(200);

    assertDatabaseHas((new Banner())->getTable(), [
        'id' => $banner->id,
        'name' => $request['name'],
    ]);
});

test('PUT /api/v1/contents/banners/{id} remove button 200', function () {
    $button = BannerButton::factory()->create();

    /** @var Banner $banner */
    $banner = Banner::factory()->for($button, 'button')->create();

    $request = BannerRequestFactory::new()->withButton(false)->make();

    putJson("/api/v1/contents/banners/{$banner->id}", $request)
        ->assertJsonPath('data.name', $request['name'])
        ->assertStatus(200);

    assertDatabaseHas((new Banner())->getTable(), [
        'id' => $banner->id,
        'name' => $request['name'],
    ]);

    assertModelMissing($button);
});

test('PUT /api/v1/contents/banners/{id} change button data 200', function () {
    /** @var BannerButton $button */
    $button = BannerButton::factory()->create();

    /** @var Banner $banner */
    $banner = Banner::factory()->for($button, 'button')->create();

    $request = BannerRequestFactory::new()->withButton(true)->make();

    putJson("/api/v1/contents/banners/{$banner->id}", $request)
        ->assertJsonPath('data.name', $request['name'])
        ->assertStatus(200);

    assertDatabaseHas((new Banner())->getTable(), [
        'id' => $banner->id,
        'name' => $request['name'],
    ]);

    assertDatabaseHas((new BannerButton())->getTable(), [
        'id' => $button->id,
        'text' => $request['button']['text'],
    ]);
});

test('PUT /api/v1/contents/banners/{id} add button 200', function () {
    /** @var Banner $banner */
    $banner = Banner::factory()->create(['button_id' => null]);

    $request = BannerRequestFactory::new()->withButton(true)->make();

    $buttonId = putJson("/api/v1/contents/banners/{$banner->id}", $request)
        ->assertOk()
        ->json('data.button.id');

    assertDatabaseHas(BannerButton::class, [
        'id' => $buttonId,
        'text' => $request['button']['text'],
    ]);
});

test('PUT /api/v1/contents/banners/{id} 404', function () {
    $request = BannerRequestFactory::new()->make();

    $undefinedId = 1;
    putJson("/api/v1/contents/banners/{$undefinedId}", $request)
        ->assertStatus(404);
});

test('DELETE /api/v1/contents/banners/{id} 200', function () {
    /** @var Banner $banner */
    $banner = Banner::factory()->create();

    deleteJson("/api/v1/contents/banners/{$banner->id}")
        ->assertStatus(200);

    assertModelMissing($banner);
});
