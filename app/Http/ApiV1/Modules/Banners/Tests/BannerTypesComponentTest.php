<?php

use App\Domain\Contents\Models\BannerType;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'contents', 'banner_types');

test('POST /api/v1/contents/banner-types:search 200', function () {
    BannerType::query()->delete();

    $bannerTypes = BannerType::factory()
        ->forEachSequence(
            ['active' => false],
            ['active' => true],
        )
        ->create();

    postJson('/api/v1/contents/banner-types:search', [
        "filter" => ["active" => true],
        "sort" => ["-id"],
    ])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $bannerTypes->last()->id)
        ->assertJsonPath('data.0.active', true);
});

test("POST /api/v1/contents/banner-types:search filter success", function (
    string $fieldKey,
    $value = null,
    ?string $filterKey = null,
    $filterValue = null
) {
    BannerType::query()->delete();

    /** @var BannerType $bannerType */
    $bannerType = BannerType::factory()->create($value ? [$fieldKey => $value] : []);
    BannerType::factory()->create();

    postJson("/api/v1/contents/banner-types:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $bannerType->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $bannerType->id);
})->with([
    ['id'],
    ['name'],
    ['name'],
    ['active'],
    ['code'],

    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/contents/banner-types:search sort success", function (string $sort) {
    BannerType::factory()->create();
    postJson("/api/v1/contents/banner-types:search", ["sort" => [$sort]])->assertOk();
})->with([
    'id',
    'name',
    'active',
    'code',
    'created_at',
    'updated_at',
]);
