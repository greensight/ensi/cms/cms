<?php

use App\Domain\Contents\Models\Banner;
use App\Http\ApiV1\Modules\Banners\Tests\Factories\UploadFileRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\BannerImageTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

use function Pest\Laravel\postJson;

function storage_fake(): Filesystem
{
    $fs = resolve(EnsiFilesystemManager::class);

    return Storage::fake($fs->publicDiskName());
}

test('POST /api/v1/contents/banners/{id}:upload-file 200', function (string $type) {
    $disk = storage_fake();

    $banner = Banner::factory()->create();
    $request = UploadFileRequestFactory::new()->make(['type' => $type]);

    $response = postFile("/api/v1/contents/banners/{$banner->id}:upload-file", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['path', 'root_path', 'url']]);

    $disk->exists($response['data']['path']);
})->with([
    [BannerImageTypeEnum::DESKTOP_IMAGE_TYPE->value],
    [BannerImageTypeEnum::MOBILE_IMAGE_TYPE->value],
]);

test('POST /api/v1/contents/banners/{id}:delete-file 200', function (string $type) {
    $disk = storage_fake();

    $banner = Banner::factory()->create();
    $request = UploadFileRequestFactory::new()->make(['type' => $type]);

    $response = postFile("/api/v1/contents/banners/{$banner->id}:upload-file", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['path', 'root_path', 'url']]);

    $disk->exists($response['data']['path']);

    postJson("/api/v1/contents/banners/{$banner->id}:delete-file", ["type" => $type])
        ->assertOk();

    $disk->assertMissing($response['data']['path']);
})->with([
    [BannerImageTypeEnum::DESKTOP_IMAGE_TYPE->value],
    [BannerImageTypeEnum::MOBILE_IMAGE_TYPE->value],
]);
