<?php

namespace App\Http\ApiV1\Modules\Banners\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\BannerButtonLocationEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BannerButtonTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class BannerRequestFactory extends BaseApiFactory
{
    protected bool $useSort = false;
    protected bool $withButton = false;

    protected function definition(): array
    {
        $data = [
            'name' => $this->faker->words(3, true),
            'is_active' => $this->faker->boolean,
            'desktop_image' => $this->faker->optional()->url,
            'mobile_image' => $this->faker->optional()->url,
            'url' => $this->faker->optional()->url,

        ];

        if ($this->useSort) {
            $data['sort'] = $this->faker->numberBetween();
        }

        if ($this->withButton) {
            $data['button'] = [
                'url' => $this->faker->url,
                'text' => $this->faker->sentence,
                'location' => $this->faker->randomElement(BannerButtonLocationEnum::cases())->value,
                'type' => $this->faker->randomElement(BannerButtonTypeEnum::cases())->value,
            ];
        }

        return $data;
    }

    public function withSort(bool $useSort = true): self
    {
        $this->useSort = $useSort;

        return $this;
    }

    public function withButton(bool $withButton = true): self
    {
        $this->withButton = $withButton;

        return $this;
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
