<?php

namespace App\Http\ApiV1\Modules\Banners\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\BannerImageTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Illuminate\Http\UploadedFile;

class UploadFileRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'file' => UploadedFile::fake()->create('file.png', kilobytes: 20),
            'type' => $this->faker->randomElement(BannerImageTypeEnum::cases()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
