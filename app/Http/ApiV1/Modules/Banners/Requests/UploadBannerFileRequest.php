<?php

namespace App\Http\ApiV1\Modules\Banners\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\BannerImageTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rules\Enum;

class UploadBannerFileRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'image', 'max:2048'],
            'type' => ['required', new Enum(BannerImageTypeEnum::class)],
        ];
    }

    public function getFile(): UploadedFile
    {
        return $this->file('file');
    }

    public function getType(): string
    {
        return $this->input('type');
    }
}
