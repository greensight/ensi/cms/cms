<?php

namespace App\Http\ApiV1\Modules\Banners\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\BannerImageTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class DeleteBannerFileRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'type' => ['required', new Enum(BannerImageTypeEnum::class)],
        ];
    }

    public function getType(): string
    {
        return $this->input('type');
    }
}
