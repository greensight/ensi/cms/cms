<?php

namespace App\Http\ApiV1\Modules\Banners\Requests;

use App\Domain\Contents\Models\BannerType;
use App\Http\ApiV1\OpenApiGenerated\Enums\BannerButtonLocationEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\BannerButtonTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class CreateBannerRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['string', 'required'],
            'is_active' => ['boolean', 'required'],
            'url' => ['string', 'nullable'],
            'sort' => ['integer', 'gte:0'],
            'type_id' => ['integer', 'nullable', Rule::exists(BannerType::class, 'id')],

            'button' => ['array', 'min:1', 'nullable'],
            'button.url' => ['string', 'required_with:button'],
            'button.text' => ['string', 'required_with:button'],
            'button.location' => ['string', 'required_with:button', new Enum(BannerButtonLocationEnum::class)],
            'button.type' => ['string', 'required_with:button', new Enum(BannerButtonTypeEnum::class)],
        ];
    }

    public function getButton(): ?array
    {
        return $this->input('button');
    }
}
