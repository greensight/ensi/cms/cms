<?php

namespace App\Http\ApiV1\Modules\Banners\Queries;

use App\Domain\Contents\Models\BannerType;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class BannerTypesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(BannerType::query());

        $this->allowedSorts([
            'id',
            'name',
            'active',
            'code',
            'created_at',
            'updated_at',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('code'),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('id');
    }
}
