<?php

namespace App\Http\ApiV1\Modules\Banners\Queries;

use App\Domain\Contents\Models\Banner;
use App\Http\ApiV1\Support\Sorts\AdditionalSort;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class BannersQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Banner::query());

        $this->allowedIncludes(['type', 'button']);

        $this->allowedSorts([
            'id',
            'name',
            'code',
            'is_active',
            'created_at',
            'updated_at',

            AllowedSort::custom('sort', new AdditionalSort('created_at', true)),
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            ...StringFilter::make('name')->exact()->contain(),
            ...StringFilter::make('code')->exact()->contain(),
            AllowedFilter::exact('is_active'),
            AllowedFilter::exact('type_id'),
            AllowedFilter::exact('button_id'),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('-id');
    }

    public function withButton(): self
    {
        $this->with(['button']);

        return $this;
    }
}
