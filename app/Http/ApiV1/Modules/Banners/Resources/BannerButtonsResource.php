<?php

namespace App\Http\ApiV1\Modules\Banners\Resources;

use App\Domain\Contents\Models\BannerButton;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin BannerButton */
class BannerButtonsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'url' => $this->url,
            'text' => $this->text,
            'location' => $this->location,
            'type' => $this->type,
        ];
    }
}
