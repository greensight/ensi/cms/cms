<?php

namespace App\Http\ApiV1\Modules\Banners\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;

/** @mixin EnsiFile */
class BannerFilesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'path' => $this->getPath(),
            'root_path' => $this->getRootPath(),
            'url' => $this->getUrl(),
        ];
    }
}
