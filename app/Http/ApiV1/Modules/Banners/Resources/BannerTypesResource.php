<?php

namespace App\Http\ApiV1\Modules\Banners\Resources;

use App\Domain\Contents\Models\BannerType;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin BannerType */
class BannerTypesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'active' => $this->active,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
