<?php

namespace App\Http\ApiV1\Modules\Banners\Resources;

use App\Domain\Contents\Models\Banner;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Banner */
class BannersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'is_active' => $this->is_active,
            'url' => $this->url,
            'sort' => $this->sort,
            'desktop_image' => $this->mapPublicFileToResponse($this->desktop_image),
            'mobile_image' => $this->mapPublicFileToResponse($this->mobile_image),
            'type_id' => $this->type_id,
            'button_id' => $this->button_id,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'type' => new BannerTypesResource($this->whenLoaded('type')),
            'button' => new BannerButtonsResource($this->whenLoaded('button')),
        ];
    }
}
