<?php

namespace App\Http\ApiV1\Modules\Products\Controllers;

use App\Domain\Nameplates\Actions\AddProductNameplatesAction;
use App\Domain\Nameplates\Actions\DeleteProductNameplatesAction;
use App\Http\ApiV1\Modules\Products\Requests\AddProductNameplatesRequest;
use App\Http\ApiV1\Modules\Products\Requests\DeleteProductNameplatesRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class ProductsController
{
    public function addNameplates(int $productId, AddProductNameplatesRequest $request, AddProductNameplatesAction $action): Responsable
    {
        $action->execute($productId, $request->getNameplateIds());

        return new EmptyResource();
    }

    public function deleteNameplates(int $productId, DeleteProductNameplatesRequest $request, DeleteProductNameplatesAction $action): Responsable
    {
        $action->execute($productId, $request->getNameplateIds());

        return new EmptyResource();
    }
}
