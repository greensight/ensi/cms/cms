<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Domain\Nameplates\Models\Nameplate;
use App\Domain\Nameplates\Models\NameplateProduct;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class AddProductNameplatesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $productId = (int)$this->route('id');

        return [
            'ids' => ['required', 'array', 'min:1'],
            'ids.*' => ['integer', 'distinct',
                Rule::exists(Nameplate::class, 'id'),
                Rule::unique(NameplateProduct::class, 'nameplate_id')
                    ->where('product_id', $productId),
            ],
        ];
    }

    public function getNameplateIds(): array
    {
        return $this->validated('ids');
    }
}
