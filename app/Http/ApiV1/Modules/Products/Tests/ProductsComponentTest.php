<?php

use App\Domain\Kafka\Actions\Send\SendNameplateEventAction;
use App\Domain\Kafka\Actions\Send\SendNameplateProductEventAction;
use App\Domain\Nameplates\Models\Nameplate;
use App\Domain\Nameplates\Models\NameplateProduct;
use App\Domain\Support\Tests\Factories\Pim\ProductFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use App\Http\ApiV1\Support\Tests\Factories\RelationIdsRequestFactory;
use Ensi\PimClient\ApiException;
use Illuminate\Support\Collection;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/products/products/{id}:add-nameplates 200', function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldReceive('execute');

    $productId = 1;
    $this->mockPimProductsApi()->allows([
        'getProduct' => ProductFactory::new()->makeResponse(['id' => $productId]),
    ]);

    /** @var Collection<Nameplate> $nameplates */
    $nameplates = Nameplate::factory()
        ->count(3)
        ->create();

    $request = RelationIdsRequestFactory::new()->fillIds($nameplates->pluck('id')->all())->make();

    postJson("/api/v1/products/products/{$productId}:add-nameplates", $request)
        ->assertStatus(200);

    foreach ($request['ids'] as $nameplateId) {
        assertDatabaseHas(
            NameplateProduct::class,
            ['nameplate_id' => $nameplateId, 'product_id' => $productId]
        );
    }
});

test('POST /api/v1/products/products/{id}:add-nameplates 400 already exists', function () {
    $this->mock(SendNameplateEventAction::class)->shouldNotReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldNotReceive('execute');

    $productId = 1;
    $this->mockPimProductsApi()->allows([
        'getProduct' => ProductFactory::new()->makeResponse(['id' => $productId]),
    ]);

    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->createQuietly();
    NameplateProduct::factory()->for($nameplate)->createQuietly(['product_id' => $productId]);

    $request = RelationIdsRequestFactory::new()->fillIds([$nameplate->id])->make();

    postJson("/api/v1/products/products/{$productId}:add-nameplates", $request)
        ->assertStatus(400);
});

test('POST /api/v1/products/products/{id}:add-nameplates error fetching product', function (int $code, int $responseCode) {
    $this->mock(SendNameplateEventAction::class)->shouldNotReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldNotReceive('execute');

    /** @var ApiV1ComponentTestCase $this */
    $this->mockPimProductsApi()
        ->shouldReceive('getProduct')
        ->andThrow(new ApiException(code: $code));

    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->createQuietly();

    $request = RelationIdsRequestFactory::new()->fillIds([$nameplate->id])->make();

    postJson("/api/v1/products/products/{$code}:add-nameplates", $request)
        ->assertStatus($responseCode);
})->with([
    [503, 400],
    [404, 404],
]);

test('POST /api/v1/products/products/{id}:add-nameplates 400 nameplates not existing', function () {
    $this->mock(SendNameplateEventAction::class)->shouldNotReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldNotReceive('execute');

    postJson("/api/v1/products/products/404:add-nameplates", RelationIdsRequestFactory::new()->make())
        ->assertStatus(400);
});

test('DELETE /api/v1/products/products/{id}:delete-nameplates 200', function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldReceive('execute');

    $productId = 1;
    /** @var Collection<Nameplate> $nameplates */
    $nameplates = Nameplate::factory()
        ->count(3)
        ->create();

    /** @var array|NameplateProduct[] $nameplateProductLinks */
    $nameplateProductLinks = [];
    foreach ($nameplates as $nameplate) {
        $nameplateProductLinks[] = NameplateProduct::factory()->for($nameplate)->create(['product_id' => $productId]);
    }

    /** @var NameplateProduct $nameplateProductLinkAnother */
    $nameplateProductLinkAnother = NameplateProduct::factory()->create(['product_id' => $productId + 1]);

    $request = RelationIdsRequestFactory::new()->fillIds($nameplates->pluck('id')->all())->make();

    deleteJson("/api/v1/products/products/{$productId}:delete-nameplates", $request)
        ->assertStatus(200);

    assertDatabaseHas(NameplateProduct::class, ['id' => $nameplateProductLinkAnother->id]);

    foreach ($nameplateProductLinks as $nameplateProductLink) {
        assertDatabaseMissing(NameplateProduct::class, ['id' => $nameplateProductLink->id]);
    }
});
