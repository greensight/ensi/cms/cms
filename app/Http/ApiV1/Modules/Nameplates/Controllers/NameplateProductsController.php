<?php

namespace App\Http\ApiV1\Modules\Nameplates\Controllers;

use App\Http\ApiV1\Modules\Nameplates\Queries\NameplateProductsQuery;
use App\Http\ApiV1\Modules\Nameplates\Resources\NameplateProductsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class NameplateProductsController
{
    public function get(int $id, NameplateProductsQuery $query): Responsable
    {
        return new NameplateProductsResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, NameplateProductsQuery $query): Responsable
    {
        return NameplateProductsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
