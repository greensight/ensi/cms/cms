<?php

namespace App\Http\ApiV1\Modules\Nameplates\Controllers;

use App\Domain\Nameplates\Actions\AddNameplateProductsAction;
use App\Domain\Nameplates\Actions\CreateNameplateAction;
use App\Domain\Nameplates\Actions\DeleteNameplateAction;
use App\Domain\Nameplates\Actions\DeleteNameplateProductsAction;
use App\Domain\Nameplates\Actions\PatchNameplateAction;
use App\Http\ApiV1\Modules\Nameplates\Queries\NameplatesQuery;
use App\Http\ApiV1\Modules\Nameplates\Requests\AddNameplateProductsRequest;
use App\Http\ApiV1\Modules\Nameplates\Requests\CreateNameplateRequest;
use App\Http\ApiV1\Modules\Nameplates\Requests\DeleteNameplateProductsRequest;
use App\Http\ApiV1\Modules\Nameplates\Requests\PatchNameplateRequest;
use App\Http\ApiV1\Modules\Nameplates\Resources\NameplatesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class NameplatesController
{
    public function get(int $id, NameplatesQuery $query): Responsable
    {
        return new NameplatesResource($query->findOrFail($id));
    }

    public function create(CreateNameplateRequest $request, CreateNameplateAction $action): Responsable
    {
        return new NameplatesResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchNameplateRequest $request, PatchNameplateAction $action): Responsable
    {
        return new NameplatesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteNameplateAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, NameplatesQuery $query): Responsable
    {
        return NameplatesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function addProducts(int $id, NameplatesQuery $query, AddNameplateProductsRequest $request, AddNameplateProductsAction $action): Responsable
    {
        $action->execute($id, $query->getEloquentBuilder(), $request->getProductIds());

        return new EmptyResource();
    }

    public function deleteProducts(int $id, DeleteNameplateProductsRequest $request, DeleteNameplateProductsAction $action): Responsable
    {
        $action->execute($id, $request->getProductIds());

        return new EmptyResource();
    }
}
