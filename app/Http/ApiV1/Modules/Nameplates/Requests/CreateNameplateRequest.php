<?php

namespace App\Http\ApiV1\Modules\Nameplates\Requests;

use App\Domain\Nameplates\Models\Nameplate;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateNameplateRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'code' => ['required', 'string', Rule::unique(Nameplate::class)],
            'text_color' => ['required', 'string'],
            'background_color' => ['required', 'string'],
            'is_active' => ['required', 'boolean'],
        ];
    }
}
