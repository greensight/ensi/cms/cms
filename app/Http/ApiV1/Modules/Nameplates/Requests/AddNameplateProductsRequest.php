<?php

namespace App\Http\ApiV1\Modules\Nameplates\Requests;

use App\Domain\Nameplates\Models\NameplateProduct;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class AddNameplateProductsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $nameplateId = (int)$this->route('id');

        return [
            'ids' => ['required', 'array', 'min:1'],
            'ids.*' => ['integer', 'distinct',
                Rule::unique(NameplateProduct::class, 'product_id')
                    ->where('nameplate_id', $nameplateId),
            ],
        ];
    }

    public function getProductIds(): array
    {
        return $this->validated('ids');
    }
}
