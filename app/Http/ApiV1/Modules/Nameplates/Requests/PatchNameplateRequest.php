<?php

namespace App\Http\ApiV1\Modules\Nameplates\Requests;

use App\Domain\Nameplates\Models\Nameplate;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchNameplateRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'string'],
            'code' => ['sometimes', 'string', Rule::unique(Nameplate::class)->ignore((int)$this->route('id'))],
            'text_color' => ['sometimes', 'string'],
            'background_color' => ['sometimes', 'string'],
            'is_active' => ['sometimes', 'boolean'],
        ];
    }
}
