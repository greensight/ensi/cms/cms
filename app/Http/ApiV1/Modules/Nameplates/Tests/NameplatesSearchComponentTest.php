<?php

use App\Domain\Kafka\Actions\Send\SendNameplateEventAction;
use App\Domain\Kafka\Actions\Send\SendNameplateProductEventAction;
use App\Domain\Nameplates\Models\Nameplate;
use App\Domain\Nameplates\Models\NameplateProduct;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Illuminate\Support\Collection;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'nameplates');


test('POST /api/v1/nameplates/nameplates:search 200', function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');

    /** @var Collection<Nameplate> $nameplates */
    $nameplates = Nameplate::factory()
        ->count(10)
        ->sequence(
            ['is_active' => true],
            ['is_active' => false],
        )
        ->create();

    postJson('/api/v1/nameplates/nameplates:search', [
        "filter" => ["is_active" => false],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $nameplates->last()->id)
        ->assertJsonPath('data.0.is_active', false);
});

test("POST /api/v1/nameplates/nameplates:search filter success", function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');

    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->create($value ? [$fieldKey => $value] : []);
    Nameplate::factory()->create();

    postJson("/api/v1/nameplates/nameplates:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $nameplate->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $nameplate->id);
})->with([
    ['id'],
    ['name', 'Эксклюзивно в "Ensi"!', 'name_like', 'ensi'],
    ['is_active', true, 'is_active', true],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/nameplates/nameplates:search product_id filter success", function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldReceive('execute');

    /** @var NameplateProduct $nameplateProductLink */
    $nameplateProductLink = NameplateProduct::factory()->create();

    /** @var NameplateProduct $nameplateProductLinkAnother */
    $nameplateProductLinkAnother = NameplateProduct::factory()->create();

    postJson("/api/v1/nameplates/nameplates:search", ["filter" => ['product_id' => $nameplateProductLink->product_id]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $nameplateProductLink->nameplate->id);
});

test("POST /api/v1/nameplates/nameplates:search sort success", function (string $sort) {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');

    Nameplate::factory()->create();
    postJson("/api/v1/nameplates/nameplates:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'name',
    'is_active',
    'created_at',
    'updated_at',
]);

test("POST /api/v1/nameplates/nameplates:search include success", function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldReceive('execute');

    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->create();
    /** @var NameplateProduct $nameplateProductLink */
    $nameplateProductLink = NameplateProduct::factory()->for($nameplate)->create();

    postJson("/api/v1/nameplates/nameplates:search", ["include" => ['product_links']])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data.0.product_links')
        ->assertJsonPath('data.0.product_links.0.id', $nameplateProductLink->id);
});
