<?php

use App\Domain\Kafka\Actions\Send\SendNameplateEventAction;
use App\Domain\Kafka\Actions\Send\SendNameplateProductEventAction;
use App\Domain\Nameplates\Models\Nameplate;
use App\Domain\Nameplates\Models\NameplateProduct;
use App\Domain\Support\Tests\Factories\Pim\ProductFactory;
use App\Http\ApiV1\Modules\Nameplates\Tests\Factories\NameplateRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use App\Http\ApiV1\Support\Tests\Factories\RelationIdsRequestFactory;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'nameplates');

test('POST /api/v1/nameplates/nameplates 201', function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute')->once();

    $request = NameplateRequestFactory::new()->make();

    postJson('/api/v1/nameplates/nameplates', $request)
        ->assertStatus(201);

    assertDatabaseHas(Nameplate::class, ['code' => $request['code']]);
});

test('POST /api/v1/nameplates/nameplates 400', function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');
    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->create();
    $request = NameplateRequestFactory::new()->make(['code' => $nameplate->code]);

    postJson('/api/v1/nameplates/nameplates', $request)
        ->assertStatus(400);
});

test('GET /api/v1/nameplates/nameplates/{id} 200', function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');
    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->create();

    getJson("/api/v1/nameplates/nameplates/{$nameplate->id}")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['id', 'name', 'code', 'is_active']]);
});

test('GET /api/v1/nameplates/nameplates/{id} 404', function () {
    getJson('/api/v1/nameplates/nameplates/404')
        ->assertStatus(404);
});

test('DELETE /api/v1/nameplates/nameplates/{id} 200', function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');

    /** @var Nameplate $nameplateDelete */
    $nameplateDelete = Nameplate::factory()->create();
    /** @var Nameplate $nameplateOk */
    $nameplateOk = Nameplate::factory()->create();

    deleteJson("/api/v1/nameplates/nameplates/{$nameplateDelete->id}")
        ->assertStatus(200);

    assertDatabaseHas(Nameplate::class, ['id' => $nameplateOk->id]);
    assertDatabaseMissing(Nameplate::class, ['id' => $nameplateDelete->id]);
});

test('DELETE /api/v1/nameplates/nameplates/{id} 404', function () {
    deleteJson('/api/v1/nameplates/nameplates/404')
        ->assertStatus(404);
});

test('PATCH /api/v1/nameplates/nameplates/{id} 200', function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');

    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->create();

    $request = NameplateRequestFactory::new()->make();

    patchJson("/api/v1/nameplates/nameplates/{$nameplate->id}", $request)
        ->assertStatus(200);

    assertDatabaseHas(Nameplate::class, ['id' => $nameplate->id, 'name' => $request['name']]);
});

test('PATCH /api/v1/nameplates/nameplates/{id} 404', function () {
    patchJson('/api/v1/nameplates/nameplates/404')
        ->assertStatus(404);
});

test('POST /api/v1/nameplates/nameplates/{id}:add-products 200', function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldReceive('execute');

    $products = ProductFactory::new()->makeResponseSearch(count: 2);
    $productIds = collect($products->getData())->pluck('id')->toArray();

    $this->mockPimProductsApi()->allows([
        'searchProducts' => $products,
    ]);

    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->create();

    $request = RelationIdsRequestFactory::new()->fillIds($productIds)->make();

    postJson("/api/v1/nameplates/nameplates/{$nameplate->id}:add-products", $request)
        ->assertStatus(200);

    foreach ($productIds as $productId) {
        assertDatabaseHas(
            NameplateProduct::class,
            ['nameplate_id' => $nameplate->id, 'product_id' => $productId]
        );
    }
});

test('POST /api/v1/nameplates/nameplates/{id}:add-products 400 already exists', function () {
    $this->mock(SendNameplateEventAction::class)->shouldNotReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldNotReceive('execute');

    $productId = 1;
    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseSearch(['id' => $productId]),
    ]);

    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->createQuietly();
    NameplateProduct::factory()->for($nameplate)->createQuietly(['product_id' => $productId]);

    $request = RelationIdsRequestFactory::new()->fillIds([$productId])->make();

    postJson("/api/v1/nameplates/nameplates/{$nameplate->id}:add-products", $request)
        ->assertStatus(400);
});

test('POST /api/v1/nameplates/nameplates/{id}:add-products 400 product invalid', function () {
    $this->mock(SendNameplateEventAction::class)->shouldNotReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldNotReceive('execute');

    $productId = 1;
    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseSearch(count: 0),
    ]);

    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->createQuietly();

    $request = RelationIdsRequestFactory::new()->fillIds([$productId])->make();

    postJson("/api/v1/nameplates/nameplates/{$nameplate->id}:add-products", $request)
        ->assertStatus(400);
});

test('POST /api/v1/nameplates/nameplates/{id}:add-products 404', function () {
    $this->mock(SendNameplateEventAction::class)->shouldNotReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldNotReceive('execute');

    postJson("/api/v1/nameplates/nameplates/404:add-products", RelationIdsRequestFactory::new()->make())
        ->assertStatus(404);
});

test('DELETE /api/v1/nameplates/nameplates/{id}:delete-products 200', function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldReceive('execute');

    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->create();
    $productIds = [2, 3];

    /** @var array|NameplateProduct[] $nameplateProductLinks */
    $nameplateProductLinks = [];
    foreach ($productIds as $productId) {
        $nameplateProductLinks[] = NameplateProduct::factory()->for($nameplate)->create(['product_id' => $productId]);
    }

    /** @var Nameplate $nameplateAnother */
    $nameplateAnother = Nameplate::factory()->create();
    /** @var NameplateProduct $nameplateProductLinkAnother */
    $nameplateProductLinkAnother = NameplateProduct::factory()->for($nameplateAnother)->create();

    $request = RelationIdsRequestFactory::new()->fillIds($productIds)->make();

    deleteJson("/api/v1/nameplates/nameplates/{$nameplate->id}:delete-products", $request)
        ->assertStatus(200);

    assertDatabaseHas(NameplateProduct::class, ['id' => $nameplateProductLinkAnother->id]);

    foreach ($nameplateProductLinks as $nameplateProductLink) {
        assertDatabaseMissing(NameplateProduct::class, ['id' => $nameplateProductLink->id]);
    }
});
