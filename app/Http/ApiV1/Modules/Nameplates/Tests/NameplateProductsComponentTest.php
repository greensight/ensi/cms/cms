<?php

use App\Domain\Kafka\Actions\Send\SendNameplateEventAction;
use App\Domain\Kafka\Actions\Send\SendNameplateProductEventAction;
use App\Domain\Nameplates\Models\Nameplate;
use App\Domain\Nameplates\Models\NameplateProduct;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/nameplates/nameplate-products/{id} 200', function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldReceive('execute');

    /** @var NameplateProduct $productNameplateLink */
    $productNameplateLink = NameplateProduct::factory()->create();

    getJson("/api/v1/nameplates/nameplate-products/{$productNameplateLink->id}")
        ->assertJsonPath('data.product_id', $productNameplateLink->product_id)
        ->assertStatus(200);
});

test('GET /api/v1/nameplates/nameplate-products/{id} 404', function () {
    getJson('/api/v1/nameplates/nameplate-products/404')
        ->assertStatus(404);
});

test('POST /api/v1/nameplates/nameplate-products:search 200', function () {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldReceive('execute');

    /** @var Nameplate $nameplate1 */
    $nameplate1 = Nameplate::factory()->create();
    /** @var Nameplate $nameplate2 */
    $nameplate2 = Nameplate::factory()->create();
    $productNameplateLinks = NameplateProduct::factory()
        ->count(10)
        ->sequence(
            ['nameplate_id' => $nameplate1->id],
            ['nameplate_id' => $nameplate2->id],
        )
        ->create();

    postJson('/api/v1/nameplates/nameplate-products:search', [
        "filter" => ["nameplate_id" => $nameplate2->id],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $productNameplateLinks->last()->id)
        ->assertJsonPath('data.0.nameplate_id', $nameplate2->id);
});

test("POST /api/v1/nameplates/nameplate-products:search filter success", function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldReceive('execute');

    /** @var NameplateProduct $productNameplateLink */
    $productNameplateLink = NameplateProduct::factory()->create($value ? [$fieldKey => $value] : []);
    NameplateProduct::factory()->create();

    postJson("/api/v1/nameplates/nameplate-products:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $productNameplateLink->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $productNameplateLink->id);
})->with([
    ['id'],
    ['nameplate_id'],
    ['product_id', 1],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/nameplates/nameplate-products:search sort success", function (string $sort) {
    $this->mock(SendNameplateEventAction::class)->shouldReceive('execute');
    $this->mock(SendNameplateProductEventAction::class)->shouldReceive('execute');

    NameplateProduct::factory()->create();
    postJson("/api/v1/nameplates/nameplate-products:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id', 'nameplate_id', 'product_id', 'created_at', 'updated_at',
]);
