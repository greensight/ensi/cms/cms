<?php

namespace App\Http\ApiV1\Modules\Nameplates\Resources;

use App\Domain\Nameplates\Models\Nameplate;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Nameplate */
class NameplatesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,

            'name' => $this->name,
            'code' => $this->code,

            'background_color' => $this->background_color,
            'text_color' => $this->text_color,

            'is_active' => $this->is_active,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'product_links' => NameplateProductsResource::collection($this->whenLoaded('products')),
        ];
    }
}
