<?php

namespace App\Http\ApiV1\Modules\Nameplates\Resources;

use App\Domain\Nameplates\Models\NameplateProduct;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin NameplateProduct */
class NameplateProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'nameplate_id' => $this->nameplate_id,
            'product_id' => $this->product_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
