<?php

namespace App\Http\ApiV1\Modules\Nameplates\Queries;

use App\Domain\Nameplates\Models\Nameplate;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\ExtraFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class NameplatesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Nameplate::query());

        $this->allowedSorts(['id', 'name', 'is_active', 'created_at', 'updated_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('is_active'),

            ...StringFilter::make('name')->contain(),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),

            ...ExtraFilter::nested('products', [
                AllowedFilter::exact('product_id'),
            ]),
        ]);

        $this->allowedIncludes([
            AllowedInclude::relationship('product_links', 'products'),
        ]);

        $this->defaultSort('id');
    }
}
