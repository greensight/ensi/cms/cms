<?php

namespace App\Http\ApiV1\Modules\Nameplates\Queries;

use App\Domain\Nameplates\Models\NameplateProduct;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class NameplateProductsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(NameplateProduct::query());

        $this->allowedSorts(['id', 'nameplate_id', 'product_id', 'created_at', 'updated_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('nameplate_id'),
            AllowedFilter::exact('product_id'),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('id');
    }
}
