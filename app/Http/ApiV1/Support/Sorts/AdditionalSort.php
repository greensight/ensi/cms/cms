<?php

namespace App\Http\ApiV1\Support\Sorts;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Sorts\Sort;

class AdditionalSort implements Sort
{
    public function __construct(
        protected string $additionalProperty,
        protected bool $additionalDescending
    ) {
    }

    public function __invoke(Builder $query, bool $descending, string $property): void
    {
        $query->orderBy($property, $this->getDirection($descending));
        $query->orderBy($this->additionalProperty, $this->getDirection($this->additionalDescending));
    }

    protected function getDirection(bool $descending): string
    {
        return $descending ? 'DESC' : 'ASC';
    }
}
