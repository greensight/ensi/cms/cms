<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('nameplates', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->string('code')->unique();

            $table->string('text_color');
            $table->string('background_color');

            $table->boolean('is_active');

            $table->timestamps(6);
        });

        Schema::create('product_nameplate_links', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('nameplate_id');
            $table->unsignedBigInteger('product_id');

            $table->timestamps(6);

            $table->unique(['nameplate_id', 'product_id']);

            $table->foreign('nameplate_id')
                ->references('id')
                ->on('nameplates')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('product_nameplate_links');
        Schema::dropIfExists('nameplates');
    }
};
