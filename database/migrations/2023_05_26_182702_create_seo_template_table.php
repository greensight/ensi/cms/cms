<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('seo_templates', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->tinyInteger('type');
            $table->string('header');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->text('seo_text')->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamps(6);
        });

        Schema::create('seo_template_products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id');
            $table->bigInteger('template_id');
            $table->timestamps(6);

            $table->foreign('template_id')
                ->references('id')->on('seo_templates')->cascadeOnDelete();

            $table->unique(['product_id', 'template_id']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('seo_template_products');
        Schema::dropIfExists('seo_templates');
    }
};
