<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up()
    {
        DB::table('banners')->update(['desktop_image' => null, 'tablet_image' => null, 'mobile_image' => null]);
        DB::table('product_groups')->update(['preview_photo' => null]);
        Schema::table('banners', function (Blueprint $table) {
            $table->string('desktop_image')->nullable()->change();
            $table->string('tablet_image')->nullable()->change();
            $table->string('mobile_image')->nullable()->change();
        });
        Schema::table('product_groups', function (Blueprint $table) {
            $table->string('preview_photo')->nullable()->change();
        });
    }

    public function down()
    {
        DB::table('banners')->update(['desktop_image' => null, 'tablet_image' => null, 'mobile_image' => null]);
        DB::table('product_groups')->update(['preview_photo' => null]);
        DB::statement('alter table banners alter column desktop_image type jsonb using desktop_image::jsonb');
        DB::statement('alter table banners alter column tablet_image type jsonb using tablet_image::jsonb');
        DB::statement('alter table banners alter column mobile_image type jsonb using mobile_image::jsonb');
        DB::statement('alter table product_groups alter column preview_photo type jsonb using preview_photo::jsonb');
    }
};
