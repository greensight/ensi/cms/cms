<?php

use App\Domain\Contents\Models\Menu;
use App\Domain\Contents\Models\MenuItem;
use Illuminate\Database\Migrations\Migration;

return new class () extends Migration {
    public const MENUS = [
        [
            'name' => 'Главное меню в шапке',
            'code' => Menu::HEADER_MAIN_MENU_CODE,
            'items' => self::MENU_ITEMS_OF_HEADER_MAIN_MENU,
        ],
        [
            'name' => 'Меню помощи в шапке',
            'code' => Menu::HEADER_HELP_MENU_CODE,
            'items' => self::MENU_ITEMS_OF_HEADER_HELP_MENU,
        ],
        [
            'name' => 'Главное меню в подвале',
            'code' => Menu::FOOTER_MAIN_MENU_CODE,
            'items' => self::MENU_ITEMS_OF_FOOTER_MAIN_MENU,
        ],
    ];

    public const MENU_ITEMS_OF_HEADER_MAIN_MENU = [
        [
            'name' => 'Каталог',
            'url' => '/catalog',
        ],
        [
            'name' => 'Новинки',
            'url' => '/new',
        ],
        [
            'name' => 'Акции',
            'url' => '/promo',
        ],
        [
            'name' => 'Подборки',
            'url' => '/sets',
        ],
        [
            'name' => 'Бренды',
            'url' => '/brands',
        ],
        [
            'name' => 'Мастерклассы',
            'url' => '/masterclasses',
        ],
    ];

    public const MENU_ITEMS_OF_HEADER_HELP_MENU = [
        [
            'name' => 'Доставка и оплата',
            'url' => '/',
        ],
        [
            'name' => 'Возврат товара',
            'url' => '/',
        ],
        [
            'name' => 'Гарантии',
            'url' => '/',
        ],
    ];

    public const MENU_ITEMS_OF_FOOTER_MAIN_MENU = [
        [
            'name' => 'Как купить',
            'url' => '',
            'children' => [
                [
                    'name' => 'Оплата и доставка',
                    'url' => '/',
                ],
                [
                    'name' => 'Возврат товара',
                    'url' => '/',
                ],
                [
                    'name' => 'Гарантии',
                    'url' => '/',
                ],
                [
                    'name' => 'Вопросы и ответы',
                    'url' => '/',
                ],
            ],
        ],
        [
            'name' => 'О компании',
            'url' => '',
            'children' => [
                [
                    'name' => 'О нас',
                    'url' => '/',
                ],
                [
                    'name' => 'Партнёрам',
                    'url' => '/',
                ],
                [
                    'name' => 'Обратная связь',
                    'url' => '/',
                ],
                [
                    'name' => 'Вакансии',
                    'url' => '/',
                ],
            ],
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (self::MENUS as $menu) {
            $menuModel = new Menu();
            $menuModel->name = $menu['name'];
            $menuModel->code = $menu['code'];
            $menuModel->save();

            if (!empty($menu['items'])) {
                $this->createMenuItems($menuModel->id, $menu['items']);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        MenuItem::query()->delete();
        Menu::query()->delete();
    }

    /**
     * @param int $menuId - id меню
     * @param array $items - пункты меню
     * @param int $parentItemId - id родительской категории
     */
    protected function createMenuItems(int $menuId, array $items, int $parentItemId = 0)
    {
        foreach ($items as $item) {
            $modelMenuItem = new MenuItem();
            $modelMenuItem->menu_id = $menuId;
            $modelMenuItem->name = $item['name'];
            $modelMenuItem->url = $item['url'];

            if (!empty($parentItemId)) {
                $modelMenuItem->parent_id = $parentItemId;
            }
            $modelMenuItem->save();

            if (!empty($item['children'])) {
                $this->createMenuItems($menuId, $item['children'], $modelMenuItem->id);
            }
        }
    }
};
