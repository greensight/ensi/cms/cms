<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public const PRODUCT_GROUPS_TABLE = 'product_groups';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::PRODUCT_GROUPS_TABLE, function (Blueprint $table) {
            $table->bigInteger('banner_id')->unsigned()->nullable()->default(null)->after('type_id');

            $table->foreign('banner_id')->references('id')->on('banners');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::PRODUCT_GROUPS_TABLE, function (Blueprint $table) {
            $table->dropColumn('banner_id');
        });
    }
};
