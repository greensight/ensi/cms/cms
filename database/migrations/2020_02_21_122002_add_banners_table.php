<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public const TABLE_CODE = 'banners';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_CODE, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->boolean('active')->default(true);
            $table->jsonb('desktop_image')->nullable();
            $table->jsonb('tablet_image')->nullable();
            $table->jsonb('mobile_image')->nullable();
            $table->bigInteger('type_id')->unsigned();
            $table->bigInteger('button_id')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('banner_types');
            $table->foreign('button_id')->references('id')->on('banner_buttons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE_CODE);
    }
};
