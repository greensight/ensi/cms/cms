<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kalnoy\Nestedset\NestedSet;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('url');
            $table->boolean('active');
            $table->unsignedBigInteger('order')->default(0);

            $table->unsignedBigInteger(NestedSet::LFT)->default(0);
            $table->unsignedBigInteger(NestedSet::RGT)->default(0);
            $table->unsignedBigInteger(NestedSet::PARENT_ID)->nullable();

            $table->index(NestedSet::getDefaultColumns());

            $table->timestamps();
        });

        Schema::create('product_pim_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->bigInteger('product_category_id')->unsigned();

            $table->foreign('product_category_id')->references('id')->on('product_categories');

            $table->timestamps();
        });

        Schema::create('product_pim_category_filters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('value');
            $table->bigInteger('product_pim_category_id')->unsigned();

            $table->foreign('product_pim_category_id')->references('id')->on('product_pim_categories');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_pim_category_filters');
        Schema::dropIfExists('product_pim_categories');
        Schema::dropIfExists('product_categories');
    }
};
