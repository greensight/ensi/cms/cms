<?php

use Illuminate\Database\Migrations\Migration;

return new class () extends Migration {
    public const BANNER_TYPES_TABLE = 'banner_types';
    public const BANNERS_TABLE = 'banners';

    public const TYPES_OLD = [
        [
            'name' => 'В каталоге среди товаров',
            'code' => 'catalog',
        ],
        [
            'name' => 'В каталоге сверху',
            'code' => 'catalog_top',
        ],
        [
            'name' => 'На продуктовой странице',
            'code' => 'product_group',
        ],
        [
            'name' => 'В виджете',
            'code' => 'widget',
        ],
        [
            'name' => 'В шапке',
            'code' => 'header',
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table(self::BANNER_TYPES_TABLE)->insert([
            'name' => 'Главный баннер',
            'code' => 'main',
            'active' => true,
        ]);

        $model = DB::table(self::BANNER_TYPES_TABLE)
            ->where('code', '=', 'main')
            ->first();

        DB::table(self::BANNERS_TABLE)->update(['type_id' => $model->id]);

        DB::table(self::BANNER_TYPES_TABLE)
            ->where('id', '!=', $model->id)
            ->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (self::TYPES_OLD as $typeData) {
            DB::table(self::BANNER_TYPES_TABLE)->insert([
                'name' => $typeData['name'],
                'code' => $typeData['code'],
                'active' => true,
            ]);
        }

        $substitute = DB::table(self::BANNER_TYPES_TABLE)
            ->where('code', '!=', 'main')
            ->first();

        DB::table(self::BANNERS_TABLE)->update(['type_id' => $substitute->id]);

        DB::table(self::BANNER_TYPES_TABLE)
            ->where('code', 'main')
            ->delete();
    }
};
