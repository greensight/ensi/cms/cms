<?php

use Illuminate\Database\Migrations\Migration;

return new class () extends Migration {
    public const TYPES = [
        [
            'name' => 'В каталоге среди товаров',
            'code' => 'catalog',
        ],
        [
            'name' => 'В каталоге сверху',
            'code' => 'catalog_top',
        ],
        [
            'name' => 'На продуктовой странице',
            'code' => 'product_group',
        ],
        [
            'name' => 'В виджете',
            'code' => 'widget',
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (self::TYPES as $type) {
            DB::table('banner_types')->insert([
                'name' => $type['name'],
                'code' => $type['code'],
                'active' => true,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('banner_types')->delete();
    }
};
