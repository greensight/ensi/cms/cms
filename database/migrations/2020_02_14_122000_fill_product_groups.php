<?php

use Illuminate\Database\Migrations\Migration;

return new class () extends Migration {
    public const TYPES = [
        [
            'name' => 'Акции',
            'code' => 'promo',
        ],
        [
            'name' => 'Подборки',
            'code' => 'sets',
        ],
        [
            'name' => 'Бренды',
            'code' => 'brands',
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (self::TYPES as $type) {
            DB::table('product_group_types')->insert([
                'name' => $type['name'],
                'code' => $type['code'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('product_group_types')->delete();
    }
};
