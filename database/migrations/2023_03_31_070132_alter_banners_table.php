<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

return new class () extends Migration {
    public function up()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->renameColumn('active', 'is_active');
            $table->integer('sort')->unsigned()->default(500);
            $table->string('code')->nullable();

            $table->bigInteger('type_id')->unsigned()->nullable()->change();
        });

        $banners = DB::table('banners')->get();
        foreach ($banners as $banner) {
            $slug = Str::slug($banner->name);

            DB::table('banners')
                ->where(['id' => $banner->id])
                ->update(['code' => $slug]);
        }

        Schema::table('banners', function (Blueprint $table) {
            $table->string('code')->nullable(false)->change();
        });
    }

    public function down()
    {
        $bannerType = DB::table('banner_types')->first();
        if (!$bannerType) {
            DB::table('banner_types')->insert([
                'name' => 'Главный баннер',
                'code' => 'main',
                'active' => false,
                'created_at' => now(),
            ]);

            $bannerType = DB::table('banner_types')->first();
        }

        DB::table('banners')
            ->whereNull('type_id')
            ->update(['type_id' => $bannerType->id]);

        Schema::table('banners', function (Blueprint $table) {
            $table->bigInteger('type_id')->unsigned()->nullable(false)->change();

            $table->dropColumn('code');
            $table->dropColumn('sort');
            $table->renameColumn('is_active', 'active');
        });
    }
};
