<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kalnoy\Nestedset\NestedSet;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists('landings');

        Schema::dropIfExists('product_group_products');
        Schema::dropIfExists('product_group_filters');
        Schema::dropIfExists('product_groups');
        Schema::dropIfExists('product_group_types');

        Schema::dropIfExists('product_pim_category_filters');
        Schema::dropIfExists('product_pim_categories');
        Schema::dropIfExists('product_categories');

        Schema::rename('product_nameplate_links', 'nameplate_products');

        Schema::table('banners', function (Blueprint $table) {
            $table->dropColumn('tablet_image');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->string('tablet_image')->nullable();
        });

        Schema::rename('nameplate_products', 'product_nameplate_links');

        Schema::create('landings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('active')->default(true);
            $table->string('name');
            $table->string('code');
            $table->json('widgets');

            $table->timestamps(6);
        });

        Schema::create('product_group_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');

            $table->timestamps();
        });

        Schema::create('product_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');
            $table->boolean('active')->default(true);
            $table->boolean('is_shown')->default(true)->after('active');
            $table->jsonb('preview_photo')->nullable();
            $table->bigInteger('type_id')->unsigned();
            $table->string('category_code')->nullable();

            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('product_group_types');
        });

        Schema::create('product_group_filters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_group_id')->unsigned();
            $table->string('code');
            $table->string('value');

            $table->timestamps();

            $table->foreign('product_group_id')->references('id')->on('product_groups');
        });

        Schema::create('product_group_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_group_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->integer('sort')->unsigned()->default(500);

            $table->timestamps();

            $table->foreign('product_group_id')->references('id')->on('product_groups');
        });

        Schema::create('product_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('url');
            $table->boolean('active');
            $table->unsignedBigInteger('order')->default(0);

            $table->unsignedBigInteger(NestedSet::LFT)->default(0);
            $table->unsignedBigInteger(NestedSet::RGT)->default(0);
            $table->unsignedBigInteger(NestedSet::PARENT_ID)->nullable();

            $table->index(NestedSet::getDefaultColumns());

            $table->timestamps();
        });

        Schema::create('product_pim_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->bigInteger('product_category_id')->unsigned();

            $table->foreign('product_category_id')->references('id')->on('product_categories');

            $table->timestamps();
        });

        Schema::create('product_pim_category_filters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('value');
            $table->bigInteger('product_pim_category_id')->unsigned();

            $table->foreign('product_pim_category_id')->references('id')->on('product_pim_categories');

            $table->timestamps();
        });
    }
};
