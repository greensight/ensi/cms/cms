<?php

return [
    'catalog' => [
        'pim' => [
            'base_uri' => env('CATALOG_PIM_SERVICE_HOST') . "/api/v1",
        ],
        'catalog-cache' => [
            'base_uri' => env('CATALOG_CATALOG_CACHE_SERVICE_HOST') . "/api/v1",
        ],
    ],
];
