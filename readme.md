# Ensi CMS

## Main info

Name: CMS  
Domain: CMS  
Purpose: Content management  

## Development

Instructions describing how to deploy, launch and test the service on a local machine can be found in a separate document at [Gitlab Pages](https://ensi-platform.gitlab.io/docs/tech/back)

The regulations for working on tasks are also in [Gitlab Pages](https://ensi-platform.gitlab.io/docs/guid/regulations)

## Service structure

You can read about the service structure [here](https://docs.ensi.tech/backend-guides/principles/service-structure)

## Dependencies

| Name              | Description                                                                                                                                                                                                     | Environment variables                                                                                                                          |
|-------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| PostgreSQL        | Service database                                                                                                                                                                                                | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD                                                          |
| Kafka             | Message broker. <br/>Producer sends messages to following topics:<br/> - `<contour>..cms.fact.nameplates.1`<br/> - `<contour>..cms.fact.nameplate-product-links.1`.<br/> Consumer doesn't listen to any topics. | KAFKA_CONTOUR<br/>KAFKA_BROKER_LIST<br/>KAFKA_SECURITY_PROTOCOL<br/>KAFKA_SASL_MECHANISMS<br/>KAFKA_SASL_USERNAME<br/>KAFKA_SASL_PASSWORD<br/> |
| **Ensi services** | **Ensi services with which this service communicates**                                                                                                                                                          |
| Catalog           | Ensi Pim                                                                                                                                                                                                        | CATALOG_PIM_SERVICE_HOST                                                                                                                       |
| Catalog           | Ensi Catalog Cache                                                                                                                                                                                              | CATALOG_CATALOG_CACHE_SERVICE_HOST                                                                                                             |

## Environments

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-dev/job/CMS/job/cms/  
URL: https://cms-master-dev.ensi.tech/docs/oas#/  

### Preprod

N/A

### Prod

N/A

## Contacts

The team supporting this service: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email: mail@greensight.ru

## License

[Open license for the right to use the Greensight Ecom Platform (GEP) computer program](LICENSE.md).
