<?php

namespace Tests;

use Ensi\CatalogCacheClient\Api\ElasticOffersApi as CatalogCacheOffersApi;
use Ensi\PimClient\Api\ProductsApi as PimProductsApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    // =============== Catalog ===============

    // region service Pim
    public function mockPimProductsApi(): MockInterface|PimProductsApi
    {
        return $this->mock(PimProductsApi::class);
    }
    // endregion

    // region service Catalog Cache
    public function mockCatalogCacheOffersApi(): MockInterface|CatalogCacheOffersApi
    {
        return $this->mock(CatalogCacheOffersApi::class);
    }
    // endregion
}
